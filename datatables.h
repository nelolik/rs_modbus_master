#ifndef DATATABLES_H
#define DATATABLES_H

#include <QObject>
#include <QBitArray>
#include <QVector>

class DataTables
{
public:
	DataTables();
	DataTables(int discretOutputSize, int discretInputSize,
			   int analogIntputSize, int analogOutputSize);
	~DataTables();

	int getDiscreteOutputSize() { return pDiscreteOutput == nullptr ? 0 : pDiscreteOutput->size(); }
	int getDiscreteOutputCell(int number);
	bool setDiscreteOutputCell(int number, int val);
	void resizeDiscreteOutputTable(int size);

	int getDiscreteInputSize() { return pDiscreteInput == nullptr ? 0 : pDiscreteInput->size(); }
	int getDiscreteInputCell(int number);
	int setDiscreteIntputCell(int number, int val);
	void resizeDiscreteIntputTable(int size);

	int getAnalogInputSize() { return pAnalogInput == nullptr ? 0 : pAnalogInput->size(); }
	int getAnalogInputCell(int number);
	bool setAnalogInputCell(int number, int val);
	void resizeAnalogInputTable(int size);

	int getAnalogOutputSize() { return pAnalogOutput == nullptr ? 0 : pAnalogOutput->size(); }
	int getAnalogOutputCell(int number);
	bool setAnalogOutputCell(int number, int val);
	void resizeAnalogOutputTable(int size);

	QStringList getTableDescriptions() { return tablesDescritons; }

	enum TableType {
		discreteOutput = 0,
		discreteInput = 1,
		analogInput = 2,
		analogOutput = 3
	};
//	Q_ENUM(TableType)

private:
	QBitArray *pDiscreteOutput;	//Registers 1-9999
	QBitArray *pDiscreteInput;	//Registers 10001-19999
	QVector<int> *pAnalogInput;	//Registers 30001-39999
	QVector<int> *pAnalogOutput;	//Registers 40001-49999
	QStringList tablesDescritons;

};

#endif // DATATABLES_H
