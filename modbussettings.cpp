#include "modbussettings.h"
#include <QLineEdit>
#include <QLabel>
#include <QGridLayout>
#include <QVBoxLayout>
#include <QLine>
#include <QSizePolicy>
#include <QPushButton>
#include <QSpacerItem>
#include <QIntValidator>
#include <QCheckBox>

ModbusSettings::ModbusSettings(int discretOutputSize, int discretInputSize,
							   int analogIntputSize, int analogOutputSize,
							   int discretOutputMessageSize, int discretInputMessageSize,
							   int analogIntputMessageSize, int analogOutputMessageSize,
							   int transmitionTimeout, int noAnswerTimeout,
							   int remoteHostAdres, int slaveAdres, bool useInSlaveMode)
{
	editDiscreteOutputSize = new QLineEdit(QString::number(discretOutputSize));
	editDiscreteOutputSize->setValidator(new QIntValidator(this));
	editDiscreteInputSize = new QLineEdit(QString::number(discretInputSize));
	editDiscreteInputSize->setValidator(new QIntValidator(this));
	editAnalogInputSize = new QLineEdit(QString::number(analogIntputSize));
	editAnalogInputSize->setValidator(new QIntValidator(this));
	editAnalogOutputSize = new QLineEdit(QString::number(analogOutputSize));
	editAnalogOutputSize->setValidator(new QIntValidator(this));
	editDiscreteOutputMessageSize = new QLineEdit(QString::number(discretOutputMessageSize));
	editDiscreteOutputMessageSize->setValidator(new QIntValidator(this));
	editDiscreteInputMessageSize = new QLineEdit(QString::number(discretInputMessageSize));
	editDiscreteInputMessageSize->setValidator(new QIntValidator(this));
	editAnalogInputMessageSize = new QLineEdit(QString::number(analogIntputMessageSize));
	editAnalogInputMessageSize->setValidator(new QIntValidator(this));
	editAnalogOutputMessageSize = new QLineEdit(QString::number(analogOutputMessageSize));
	editAnalogOutputMessageSize->setValidator(new QIntValidator(this));
	editTransmitionTimeout = new QLineEdit(QString::number(transmitionTimeout));
	editTransmitionTimeout->setValidator(new QIntValidator(this));
	editNoAnswerTimeout = new QLineEdit(QString::number(noAnswerTimeout));
	editNoAnswerTimeout->setValidator(new QIntValidator(this));
	editHostAdres = new QLineEdit(QString::number(remoteHostAdres));
	editHostAdres->setValidator(new QIntValidator(this));
	editSlaveAdres = new QLineEdit(QString::number(slaveAdres));
	editSlaveAdres->setValidator(new QIntValidator(this));

	QLabel *lblHeaderSizes = new QLabel("Размеры таблиц");
//	lblHeaderSizes->setMargin(5);
	QLabel *lblMessageSize = new QLabel("Размер посылки");
	QLabel *lblDiscreteOut = new QLabel("Discrete output");
	QLabel *lblDiscreteIn = new QLabel("Discrete input");
	QLabel *lblAnalogIn = new QLabel("Analog input");
	QLabel *lblAnalogOut = new QLabel("Analog output");
	QLabel *lblTimeout = new QLabel("Задержка между посылками, мсек");
	QLabel *lblNoAnswerTimeout = new QLabel("Ожидание ответа, мсек");
	QLabel *lblRequestedAdres = new QLabel("Адрес удалённого хоста");
	QLabel *lblSlaveAdres = new QLabel("Адрес в режиме slave");
	QLabel *lblAnswerAsSlave = new QLabel("Использовать режим slave");

	QGridLayout *sizesLayout = new QGridLayout();
	sizesLayout->addWidget(lblHeaderSizes, 0, 1, Qt::AlignLeft);
	sizesLayout->addWidget(lblMessageSize, 0, 2, Qt::AlignLeft);
	sizesLayout->addWidget(lblDiscreteOut, 1, 0, Qt::AlignLeft);
	sizesLayout->addWidget(editDiscreteOutputSize, 1, 1, Qt::AlignLeft);
	sizesLayout->addWidget(editDiscreteOutputMessageSize, 1, 2);
	sizesLayout->addWidget(lblDiscreteIn, 2, 0, Qt::AlignLeft);
	sizesLayout->addWidget(editDiscreteInputSize, 2, 1, Qt::AlignLeft);
	sizesLayout->addWidget(editDiscreteInputMessageSize, 2, 2);
	sizesLayout->addWidget(lblAnalogIn, 3, 0, Qt::AlignLeft);
	sizesLayout->addWidget(editAnalogInputSize, 3, 1, Qt::AlignLeft);
	sizesLayout->addWidget(editAnalogInputMessageSize, 3, 2);
	sizesLayout->addWidget(lblAnalogOut, 4, 0, Qt::AlignLeft);
	sizesLayout->addWidget(editAnalogOutputSize, 4, 1, Qt::AlignLeft);
	sizesLayout->addWidget(editAnalogOutputMessageSize, 4, 2);

	QLabel *spacer = new QLabel();
	sizesLayout->setColumnStretch(0,1);
	sizesLayout->setColumnStretch(1,1);
	sizesLayout->setColumnStretch(2,1);

	QGridLayout *timeLayout = new QGridLayout();
	timeLayout->addWidget(lblTimeout, 0, 0);
	timeLayout->addWidget(editTransmitionTimeout, 0, 1);
	timeLayout->addWidget(lblNoAnswerTimeout, 1, 0);
	timeLayout->addWidget(editNoAnswerTimeout, 1, 1);
	timeLayout->addWidget(spacer, 2, 0);
	timeLayout->addWidget(lblRequestedAdres, 3, 0);
	timeLayout->addWidget(editHostAdres, 3, 1);
	timeLayout->addWidget(lblSlaveAdres, 4, 0);
	timeLayout->addWidget(editSlaveAdres, 4, 1);
	timeLayout->setColumnMinimumWidth(0, 250);

	answerAsSlaveCBx = new QCheckBox();
	answerAsSlaveCBx->setChecked(useInSlaveMode);
	QHBoxLayout *pUseSlaveLayout = new QHBoxLayout();
	pUseSlaveLayout->addWidget(answerAsSlaveCBx);
	pUseSlaveLayout->addWidget(lblAnswerAsSlave);
	pUseSlaveLayout->addSpacerItem(
				new QSpacerItem(1,1, QSizePolicy::Expanding, QSizePolicy::Fixed));

	QHBoxLayout *pButtonsLayout = new QHBoxLayout();
	QPushButton *pOkBtn = new QPushButton("Ok");
	QPushButton *pCancelBtn = new QPushButton("Cancel");
	pButtonsLayout->addSpacerItem(
				new QSpacerItem(1,1, QSizePolicy::Expanding, QSizePolicy::Fixed));
	pButtonsLayout->addWidget(pOkBtn);
	pButtonsLayout->addWidget(pCancelBtn);
	connect(pOkBtn, &QPushButton::clicked, this, &ModbusSettings::accept);
	connect(pCancelBtn, &QPushButton::clicked, this, &ModbusSettings::reject);

	QVBoxLayout *mainLayout = new QVBoxLayout(this);
//	mainLayout->addWidget(lblHeaderSizes);
	mainLayout->addLayout(sizesLayout);
	mainLayout->addSpacing(10);
	mainLayout->addLayout(timeLayout);
	mainLayout->addLayout(pUseSlaveLayout);
	mainLayout->addSpacing(10);
	mainLayout->addLayout(pButtonsLayout);

	setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
	setFixedWidth(275);
}

int ModbusSettings::getDiscreteOutputSize()
{
	return editDiscreteOutputSize->text().toInt();
}

int ModbusSettings::getDiscreteInputSize()
{
	return editDiscreteInputSize->text().toInt();
}

int ModbusSettings::getAnalogInputSize()
{
	return editAnalogInputSize->text().toInt();
}

int ModbusSettings::getAnalogOutputSize()
{
	return  editAnalogOutputSize->text().toInt();
}

int ModbusSettings::getDiscreteOutputMessageSize()
{
	return editDiscreteOutputMessageSize->text().toInt();
}

int ModbusSettings::getDiscreteInputMessageSize()
{
	return editDiscreteInputMessageSize->text().toInt();
}

int ModbusSettings::getAnalogInputMessageSize()
{
	return editAnalogInputMessageSize->text().toInt();
}

int ModbusSettings::getAnalogOutputMessageSize()
{
	return editAnalogOutputMessageSize->text().toInt();
}

int ModbusSettings::getTransmitionTimeout()
{
	return  editTransmitionTimeout->text().toInt();
}

int ModbusSettings::getNoAnswertimeout()
{
	return editNoAnswerTimeout->text().toInt();
}

int ModbusSettings::getRemoteHostAdres()
{
	return editHostAdres->text().toInt();
}

int ModbusSettings::getSlaveAdres()
{
	return editSlaveAdres->text().toInt();
}

bool ModbusSettings::getUseInSlaveMode()
{
	return answerAsSlaveCBx->checkState() == Qt::Checked ? true : false;
}
