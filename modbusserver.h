#ifndef MODBUSSERVER_H
#define MODBUSSERVER_H
#include <QThread>
#include <QMutex>
#include <QWaitCondition>
#include <QTimer>
#include "datatables.h"

class NetworkThread;
class ModbusRTU;

class ModbusServer : public QThread
{
	Q_OBJECT

public:
	enum Exchange {
		cyclicExchange = 0,
		byChangeExchange = 1
	};
	Q_ENUM(Exchange)

	ModbusServer(DataTables *tables, NetworkThread *netThread, int commandNumber,
				 ModbusServer::Exchange mode, bool startExchange,
				 int analogOutMsgSize, int analogInputMsgSize,
				 int discreteOutputMsgSize, int discreteInputMsgSize);
	~ModbusServer();

	void setNoAnswerTimout(int msec) { mNoAnswerTimeoutMs = msec; } //Time of waiting answer
	void setTransmitTimout(int msec) { mMessageTimeoutMks = msec * 1000; } //Time between messages
	void stopRun();
	QStringList getMasterCommands() { return masterCommands; }
	void setCommand(int command);
	void setMode(ModbusServer::Exchange mode);
	int getNoANswerTimeoutMs() { return mNoAnswerTimeoutMs;}
	int getMessageTimeoutMs() { return mMessageTimeoutMks / 1000;}
	int getDiscreteOutputMessageSize() { return discreteOutputMsgSize; }
	int getDiscreteInputMessageSize() { return discreteInputMsgSize; }
	int getAnalogInputMessageSize() { return analogInputMsgSize; }
	int getAnalogOutputMessageSize() { return analogOutMsgSize; }
	void setDiscreteOutputMessageSize(int newSize) { if (newSize > 0) discreteOutputMsgSize = newSize; }
	void setDiscreteInputMessageSize(int newSize) { if (newSize > 0) discreteInputMsgSize = newSize; }
	void setAnalogInputMessageSize(int newSize) { if (newSize > 0) analogInputMsgSize = newSize; }
	void setAnalogOutputMessageSize(int newSize) { if (newSize > 0) analogOutMsgSize = newSize; }
	bool isExchangeOnPause() { return mPause; }
	void waitExchangeStoped();
	bool getUseInSlaveMode() { return mUseInSlaveMode; }
	void setUseInSlaveMode(int use) { mUseInSlaveMode = use; }

signals:
	void signalDataChanged(DataTables::TableType type, int position, int count);
	void signalUpdateTimer();
	void signalStopTimer();
	void signalExchangeOnPause();
	void signalMessageTransmitted(int cmd, int adres, int length);
	void signalMessageRecieved(int cmd, int adres, int tength);

public slots:
	void slotReceiveBytes(QByteArray bytes);
	void slotOnTimeout();
	void slotOnUpdateTimer();
	void slotOnStopTimer();
	void slotSendSingleData(int command, int adres, int count);
	void slotPauseExchange(int checkState);
	void slotExchangeStoped();


private:
	DataTables *mTables;
	ModbusRTU *mModbusRTU;
	NetworkThread *pNetThred;
	bool mRunning;
	bool mWaitingAnswer;
	bool mAnswerReceived;
	bool mPause;
	bool mUseInSlaveMode;
	QStringList masterCommands;
	QTimer mTimer;
	int mNoAnswerTimeoutMs;
	int mMessageTimeoutMks;
	int mCommand;
	int mNewCommand;
	int analogInputMsgSize = 30;
	int analogOutMsgSize = 30;
	int discreteInputMsgSize = 8;
	int discreteOutputMsgSize = 10;
	int mSentMessageSize;
	int mStartAdres;
	QMutex mutex;
	QMutex mutexPause;
	QMutex mutexWaitPause;
	QWaitCondition pauseCondition;
	QWaitCondition waitPauseCondition;
	Exchange mMode;
	Exchange mNewMode;

	// QThread interface
protected:
	void run();

	void sendMessageCmd1();
	void sendMessageCmd2();
	void sendMessageCmd3();
	void sendMessageCmd4();
	void sendMessageCmd5();
	void sendMessageCmd6();
	void sendMessageCmd15();
	void sendMessageCmd16();
	void parseMasterAnswerData(int cmdNumber, QByteArray &message);
	QByteArray prepareAnswerData(char netAdres, uint cmdNumber, QByteArray &message);
	void adresAndCountConstraints(int tableSize);
	void incrementAdres(int tableSize);
};

#endif // MODBUSSERVER_H
