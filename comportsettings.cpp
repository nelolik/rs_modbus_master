#include "comportsettings.h"
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QComboBox>
#include <QLabel>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QDebug>
#include <QString>
#include <QList>

ComPortSettings::ComPortSettings(QWidget *parent, QSerialPort *openedPort) :
	QDialog (parent)
{
	ports = new QList<QSerialPort*>;
	portInfos = QSerialPortInfo::availablePorts();
	foreach (QSerialPortInfo pi, portInfos) {
		QSerialPort *port = new QSerialPort(pi);
		ports->append(port);
	}

	int count = portInfos.size();
	QList<QString> portNames;
	QList<QString> portDescriptions;
	QList<QString> baudRates;
	QList<QString> portParities;
	QList<QString> portDataBits;
	for (int i = 0; i < count; i++) {
		portNames << (portInfos)[i].portName();
		portDescriptions << (portInfos)[i].description();
	}
	foreach (qint32 baud, QSerialPortInfo::standardBaudRates()) {
		baudRates << QString::number(baud);
	}
	portParities << "NoParity" << "EvenParity" << "OddParity" << "UnknownParity";
	portDataBits << "5" << "6" << "7" << "8";

	pPortSelect = new QComboBox(this);
	pPortSelect->addItems(portNames);
	connect(pPortSelect, static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
			this, &ComPortSettings::slotNewCOMPortSelected);
	pPortDescription = new QLabel(this);
	pPortDescription->setText(portDescriptions[0]);
	pBaudRatesCBox = new QComboBox(this);
	pBaudRatesCBox->addItems(baudRates);
	connect(pBaudRatesCBox, static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
			this, &ComPortSettings::slotBaudrateChanged);
	pParityCBox = new QComboBox(this);
	pParityCBox->addItems(portParities);
	connect(pParityCBox, static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
			this, &ComPortSettings::slotParityChanged);
	pDataBitsCBox = new QComboBox(this);
	pDataBitsCBox->addItems(portDataBits);
	connect(pDataBitsCBox, static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
			this, &ComPortSettings::slotDataBitsChanged);
	pStopBitsCBox = new QComboBox(this);
	pStopBitsCBox->addItems(QStringList() << "1" << "1.5" << "2");
	connect(pStopBitsCBox, static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
			this, &ComPortSettings::slotStopBitsChanged);

	QVBoxLayout *pLayout = new QVBoxLayout(this);
	pLayout->addWidget(new QLabel("Порт:", this));
	pLayout->addWidget(pPortSelect);
	pLayout->addWidget(pPortDescription);
	pLayout->addWidget(new QLabel("Скорость:", this));
	pLayout->addWidget(pBaudRatesCBox);
	pLayout->addWidget(new QLabel("Бит чётности:", this));
	pLayout->addWidget(pParityCBox);
	pLayout->addWidget(new QLabel("Бит данных:", this));
	pLayout->addWidget(pDataBitsCBox);
	pLayout->addWidget(new QLabel("Stop bits:", this));
	pLayout->addWidget(pStopBitsCBox);

	QHBoxLayout *pButtonsLayout = new QHBoxLayout(this);
	QPushButton *pOkBtn = new QPushButton("Ok", this);
	QPushButton *pCancelBtn = new QPushButton("Cancel", this);
	pButtonsLayout->addSpacerItem(
				new QSpacerItem(1,1, QSizePolicy::Expanding, QSizePolicy::Fixed));
	pButtonsLayout->addWidget(pOkBtn);
	pButtonsLayout->addWidget(pCancelBtn);
	pLayout->addLayout(pButtonsLayout);
	connect(pOkBtn, &QPushButton::clicked, this, &ComPortSettings::accept);
	connect(pCancelBtn, &QPushButton::clicked, this, &ComPortSettings::reject);

	bool openPortExists = false;
	for (int i = 0; i < ports->size(); i++) {
		qDebug() << "Port name: " << portInfos.at(i).portName() <<
					", description: " << portInfos.at(i).description() <<
					", baud:" << ports->at(i)->baudRate() <<
					", data bits: " << ports->at(i)->dataBits() <<
					", parity bits: " << ports->at(i)->parity() <<
					", is open: "<< ports->at(i)->isOpen();

		if (openedPort != nullptr) {
			if (portInfos.at(i).portName().compare(openedPort->portName(), Qt::CaseInsensitive) == 0) {
				pPortSelect->setCurrentText(portInfos.at(i).portName());
				pPortDescription->setText(portInfos.at(i).description());
				(*ports)[i]->setBaudRate(openedPort->baudRate());
				pBaudRatesCBox->setCurrentText(QString::number(openedPort->baudRate()));
				(*ports)[i]->setParity(openedPort->parity());
				QSerialPort::Parity parity = (*ports)[i]->parity();
				pParityCBox->setCurrentIndex(parity == QSerialPort::NoParity ? 0 :
										parity == QSerialPort::EvenParity ? 1 :
										parity == QSerialPort::OddParity ? 2 : 3);
				(*ports)[i]->setDataBits(openedPort->dataBits());
				QSerialPort::DataBits bits = (*ports)[i]->dataBits();
				pDataBitsCBox->setCurrentIndex(bits == QSerialPort::Data5 ? 0 :
											bits == QSerialPort::Data6 ? 1 :
											bits == QSerialPort::Data7 ? 2 : 3);
				(*ports)[i]->setStopBits(openedPort->stopBits());
				QSerialPort::StopBits stopBits = (*ports)[i]->stopBits();
				pStopBitsCBox->setCurrentIndex(stopBits == QSerialPort::OneStop ? 0 :
												stopBits == QSerialPort::OneAndHalfStop ? 1 :
												stopBits == QSerialPort::TwoStop ? 2 : 0);
				openPortExists = true;
				break;
			}

		}
	}
	if (!openPortExists && portInfos.size() > 0) {
		pPortSelect->setCurrentText(portInfos.at(0).portName());
		pPortDescription->setText(portInfos.at(0).description());
		pBaudRatesCBox->setCurrentText(QString::number(openedPort->baudRate()));
		QSerialPort::Parity parity = ports->at(0)->parity();
		pParityCBox->setCurrentIndex(parity == QSerialPort::NoParity ? 0 :
									parity == QSerialPort::EvenParity ? 1 :
									parity == QSerialPort::OddParity ? 2 : 3);
		QSerialPort::DataBits bits = ports->at(0)->dataBits();
		pDataBitsCBox->setCurrentIndex(bits == QSerialPort::Data5 ? 0 :
									bits == QSerialPort::Data6 ? 1 :
									bits == QSerialPort::Data7 ? 2 : 3);
		QSerialPort::StopBits stopBits = ports->at(0)->stopBits();
		pStopBitsCBox->setCurrentIndex(stopBits == QSerialPort::OneStop ? 0 :
									stopBits == QSerialPort::OneAndHalfStop ? 1 :
									stopBits == QSerialPort::TwoStop ? 2 : 0);
	}
	portPosition = pPortSelect->currentIndex();
}

ComPortSettings::~ComPortSettings()
{
	foreach (QSerialPort* p, *ports) {
		delete p;
	}
	delete ports;
}

QSerialPort* ComPortSettings::getSelectedPort()
{
	QSerialPort *port = new QSerialPort(pPortSelect->currentText());
	qint32 baudRate = (pBaudRatesCBox->currentText()).toInt();
	port->setBaudRate(baudRate);
	QSerialPort::Parity parity = pParityCBox->currentIndex() == 0 ? QSerialPort::NoParity :
								pParityCBox->currentIndex() == 1 ? QSerialPort::EvenParity:
								pParityCBox->currentIndex() == 2 ? QSerialPort::OddParity :
																   QSerialPort::UnknownParity;
	port->setParity(parity);
	QSerialPort::DataBits bits = pDataBitsCBox->currentIndex() == 0 ? QSerialPort::Data5 :
								pDataBitsCBox->currentIndex() == 1 ? QSerialPort::Data6 :
								pDataBitsCBox->currentIndex() == 2 ? QSerialPort::Data7 :
								QSerialPort::Data8;
	port->setDataBits(bits);
	QSerialPort::StopBits stopBits = pStopBitsCBox->currentIndex() == 0 ? QSerialPort::OneStop :
									pStopBitsCBox->currentIndex() == 1 ? QSerialPort::OneAndHalfStop :
									pStopBitsCBox->currentIndex() == 2 ? QSerialPort::TwoStop :
																		 QSerialPort::UnknownStopBits;
	port->setStopBits(stopBits);

	return port;
//	return ports->at(portPosition);
}

void ComPortSettings::slotNewCOMPortSelected(int index)
{
	if (portInfos.size() > index && ports->size() > index) {
	   pPortSelect->setCurrentText(portInfos.at(index).portName());
	   pPortDescription->setText(portInfos.at(index).description());
//	   pBaudRatesCBox->setCurrentText(QString::number(ports->at(index)->baudRate()));
//	   QSerialPort::Parity parity = ports->at(index)->parity();
//	   pParityCBox->setCurrentIndex(parity == QSerialPort::NoParity ? 0 :
//							   parity == QSerialPort::EvenParity ? 1 :
//							   parity == QSerialPort::OddParity ? 2 : 3);
//	   QSerialPort::DataBits bits = ports->at(index)->dataBits();
//	   pDataBitsCBox->setCurrentIndex(bits == QSerialPort::Data5 ? 0 :
//								   bits == QSerialPort::Data6 ? 1 :
//								   bits == QSerialPort::Data7 ? 2 : 3);
//	   QSerialPort::StopBits stopBits = ports->at(index)->stopBits();
//	   pStopBitsCBox->setCurrentIndex(stopBits == QSerialPort::OneStop ? 0 :
//									   stopBits == QSerialPort::OneAndHalfStop ? 1 :
//									   stopBits == QSerialPort::TwoStop ? 2 : 0);
	   portPosition = index;
	}
}

void ComPortSettings::slotBaudrateChanged(int index)
{
	if (portInfos.size() > portPosition && ports->size() > portPosition) {
		qint32 baudRate = pBaudRatesCBox->itemText(index).toInt();
		QSerialPort *oldPort = (*ports)[portPosition];
		oldPort->setBaudRate(baudRate);
		ports->replace(portPosition, oldPort);
	}
}

void ComPortSettings::slotParityChanged(int index)
{
	Q_UNUSED(index)
	if (portInfos.size() > portPosition && ports->size() > portPosition) {
		QSerialPort::Parity parity = pParityCBox->currentIndex() == 0 ? QSerialPort::NoParity :
									pParityCBox->currentIndex() == 1 ? QSerialPort::EvenParity:
									pParityCBox->currentIndex() == 2 ? QSerialPort::OddParity :
																	   QSerialPort::UnknownParity;
		(*ports)[portPosition]->setParity(parity);
	}
}

void ComPortSettings::slotDataBitsChanged(int index)
{
	Q_UNUSED(index)
	if (portInfos.size() > portPosition && ports->size() > portPosition) {
		QSerialPort::DataBits bits = pDataBitsCBox->currentIndex() == 0 ? QSerialPort::Data5 :
									pDataBitsCBox->currentIndex() == 1 ? QSerialPort::Data6 :
									pDataBitsCBox->currentIndex() == 2 ? QSerialPort::Data7 :
									QSerialPort::Data8;
		(*ports)[portPosition]->setDataBits(bits);
	}
}

void ComPortSettings::slotStopBitsChanged(int index)
{
	Q_UNUSED(index)
	if (portInfos.size() > portPosition && ports->size() > portPosition) {
		QSerialPort::StopBits stopBits = pStopBitsCBox->currentIndex() == 0 ? QSerialPort::OneStop :
										pStopBitsCBox->currentIndex() == 1 ? QSerialPort::OneAndHalfStop :
										pStopBitsCBox->currentIndex() == 2 ? QSerialPort::TwoStop :
																			 QSerialPort::UnknownStopBits;
		qDebug() << "StopBits: " << stopBits;
		(*ports)[portPosition]->setStopBits(stopBits);
	}
}

