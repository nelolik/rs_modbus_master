#ifndef COMPORTSETTINGS_H
#define COMPORTSETTINGS_H
#include <QObject>
#include<QDialog>

class QSerialPort;
class QSerialPortInfo;
class QComboBox;
class QLabel;

class ComPortSettings : public QDialog
{
	Q_OBJECT
public:
	ComPortSettings(QWidget *parent, QSerialPort *openedPort);
	~ComPortSettings();
	QSerialPort *getSelectedPort();

private:
	QComboBox *pPortSelect;
	QLabel *pPortDescription;
	QComboBox *pBaudRatesCBox;
	QComboBox *pParityCBox;
	QComboBox *pDataBitsCBox;
	QComboBox *pStopBitsCBox;

	QList<QSerialPortInfo> portInfos;
	QList<QSerialPort*> *ports;
	int portPosition = 0;
private slots:
	void slotNewCOMPortSelected(int index);
	void slotBaudrateChanged(int index);
	void slotParityChanged(int index);
	void slotDataBitsChanged(int index);
	void slotStopBitsChanged(int index);
};

#endif // COMPORTSETTINGS_H
