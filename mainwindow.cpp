#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QLabel"
#include <QtSerialPort/QSerialPortInfo>
#include <QtSerialPort/QSerialPort>
#include <QAction>
#include <QFile>
#include <QDebug>
#include "networkthread.h"
#include "modbusserver.h"
#include "datatables.h"
#include "regisersvaluestablemodel.h"
#include "tableitemdelegate.h"
#include "comportsettings.h"
#include "modbussettings.h"
#include "modbusrtu.h"

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow),
	mBaudRate(QSerialPort::Baud57600), mDataBits(QSerialPort::Data8),
	mParity(QSerialPort::NoParity), mStopBits(QSerialPort::OneStop),
	mDigOutTableSize(200), mDigInTableSize(200),
	mAnOutTableSize(400), mAnInTableSize(400),
	mDigOutMessSize(30), mDigInMessSize(30),
	mAnOutMessSize(30), mAnInMessSize(30),
	mDelayBetweenMess(200), mNoAnswTimeout(5000),
	mRemoteAdres(2), mSlaveAdres(1),
	mUseSlave(true)
{
	readConfigFile();
	ui->setupUi(this);
	statusTxLbl = new QLabel;
	statusRxLbl = new QLabel;
	ui->statusBar->addWidget(statusTxLbl);
	ui->statusBar->addWidget(statusRxLbl);

	mDataTables = new DataTables(mDigOutTableSize, mDigInTableSize, mAnOutTableSize, mAnInTableSize);
	mModel = new RegisersValuesTableModel(mDataTables, DataTables::discreteOutput);
	ui->tableOutput->setModel(mModel);
	ui->tableOutput->resizeColumnsToContents();
	ui->tableOutput->setItemDelegate(new TableItemDelegate());

	QList<QSerialPortInfo> avaliablePortsInfo = QSerialPortInfo::availablePorts();
	if (avaliablePortsInfo.size() > 0) {
		bool portFound = false;
		for (QSerialPortInfo info : avaliablePortsInfo) {
			if (info.portName().compare(mPortName) == 0) {
				mOpenedPort = new QSerialPort(info);
				portFound = true;
			}
		}
		if (!portFound) {
			mOpenedPort = new QSerialPort(avaliablePortsInfo.at(0));
		}
	} else {
		mOpenedPort = new QSerialPort();
	}
	mOpenedPort->setBaudRate(mBaudRate);
	mOpenedPort->setDataBits(static_cast<QSerialPort::DataBits>(mDataBits));
	mOpenedPort->setParity(static_cast<QSerialPort::Parity>(mParity));
	mOpenedPort->setStopBits(static_cast<QSerialPort::StopBits>(mStopBits));
	netThread = new NetworkThread(mOpenedPort);
	connect(netThread, &NetworkThread::signalCOMPortChanged, this, &MainWindow::slotOnCOMPortChanged);
	netThread->start();
	mServer = new ModbusServer(mDataTables, netThread, 1, ModbusServer::cyclicExchange, false,
							   mAnOutMessSize, mAnInMessSize, mDigOutMessSize, mDigInMessSize);
	mServer->setTransmitTimout(mDelayBetweenMess);
	mServer->setNoAnswerTimout(mNoAnswTimeout);
	mServer->setUseInSlaveMode(mUseSlave);
	ModbusRTU::setHostAdres(static_cast<char>(mRemoteAdres));
	ModbusRTU::setSlaveAdres(static_cast<char>(mSlaveAdres));
	connect(mServer, &ModbusServer::signalDataChanged, mModel, &RegisersValuesTableModel::onDataChangedFronRS);
	connect(mModel, &RegisersValuesTableModel::signalUpdateRemoteData, mServer, &ModbusServer::slotSendSingleData);
	connect(netThread, &NetworkThread::signalRecievedBytes, mServer, &ModbusServer::slotReceiveBytes);
	connect(mServer, &ModbusServer::signalMessageTransmitted, this, &MainWindow::slotOnMessageTransmitted);
	connect(mServer, &ModbusServer::signalMessageRecieved, this, &MainWindow::slotOnMessageReceived);
	mServer->start();

	ui->tableTypeCmBox->addItems(mDataTables->getTableDescriptions());
	connect(ui->tableTypeCmBox, static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
			this, &MainWindow::slotOnTableTypeChanged);

	ui->messageTypeCmBox->addItems(mServer->getMasterCommands());
	connect(ui->messageTypeCmBox, static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
			this, &MainWindow::slotOnSendCommandTypeChanged);

	ui->sendModeCBox->addItems(QStringList() << "Отпралять циклически" << "Прочитать регистр");
	connect(ui->sendModeCBox, static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
			this, &MainWindow::slotOnSendModeChanged);
	ui->runMasterChekBx->setEnabled(true);
	connect(ui->runMasterChekBx, &QCheckBox::stateChanged, mServer, &ModbusServer::slotPauseExchange);
	ui->runMasterChekBx->setVisible(true);
	ui->registerNumberEdit->setVisible(false);
	ui->readRegisterButton->setVisible(false);

}

MainWindow::~MainWindow()
{
	writeConfigFile();
	netThread->stopRun();
	mServer->stopRun();
	netThread->wait(5000);
	mServer->wait(6000);
	delete netThread;
	delete mServer;
	delete mModel;
	delete mDataTables;
	delete ui;
}

QString MainWindow::fileConfig = ".\\settings.conf";
QString MainWindow::TAG_PORT = "PORT";
QString MainWindow::TAG_BAUD = "BAUD";
QString MainWindow::TAG_DATA_BITS = "DATA_BITS";
QString MainWindow::TAG_PARITY = "PARITY";
QString MainWindow::TAG_STOP_BITS = "STOP_BITS";
QString MainWindow::TAG_DO_TABLE_SIZE = "DO_TABLE_SIZE";
QString MainWindow::TAG_DI_TABLE_SIZE = "DI_TABLE_SIZE";
QString MainWindow::TAG_AO_TABLE_SIZE = "AO_TABLE_SIZE";
QString MainWindow::TAG_AI_TABLE_SIZE = "AI_TABLE_SIZE";
QString MainWindow::TAG_DO_MESS_SIZE = "DO_MESSAGE_SIZE";
QString MainWindow::TAG_DI_MESS_SIZE = "DI_MESSAGE_SIZE";
QString MainWindow::TAG_AO_MESS_SIZE = "AO_MESSAGE_SIZE";
QString MainWindow::TAG_AI_MESS_SIZE = "AI_MESSAGE_SIZE";
QString MainWindow::TAG_DELAY = "DELAY";
QString MainWindow::TAG_ANSWER_TIMEOUT = "ANSWER_TIMEOUT";
QString MainWindow::TAG_REMOTE_HOST = "REMOTE_HOST";
QString MainWindow::TAG_SLAVE_ADRES = "SLAVE_ADRES";
QString MainWindow::TAG_USE_SLAVE = "USE_SLAVE";

void MainWindow::onRSError(QString msg)
{
	Q_UNUSED(msg)
//	ui->textOutput->append(msg);
}

void MainWindow::onRSData(QString msg)
{
	Q_UNUSED(msg)
	//	ui->textOutput->append(msg);
}

void MainWindow::slotOnTableTypeChanged(int index)
{
	switch (index) {
	case 0:
		mModel->setOutputTableType(DataTables::discreteOutput);
		break;
	case 1:
		mModel->setOutputTableType(DataTables::discreteInput);
		break;
	case 2:
		mModel->setOutputTableType(DataTables::analogInput);
		break;
	case 3:
		mModel->setOutputTableType(DataTables::analogOutput);
		break;
	}
}

void MainWindow::slotOnSendCommandTypeChanged(int index)
{
	int modeIndex = ui->sendModeCBox->currentIndex();
	switch (index) {
	case 0:
		mServer->setCommand(1);
		ui->tableTypeCmBox->setCurrentIndex(0);
		ui->sendModeCBox->removeItem(1);
		ui->sendModeCBox->addItem("Прочитать регистр");
		ui->runMasterChekBx->setVisible(modeIndex == 0);
		ui->registerNumberEdit->setVisible(modeIndex != 0);
		ui->readRegisterButton->setVisible(modeIndex != 0);
		break;
	case 1:
		mServer->setCommand(2);
		ui->tableTypeCmBox->setCurrentIndex(1);
		ui->sendModeCBox->removeItem(1);
		ui->sendModeCBox->addItem("Прочитать регистр");
		ui->runMasterChekBx->setVisible(modeIndex == 0);
		ui->registerNumberEdit->setVisible(modeIndex != 0);
		ui->readRegisterButton->setVisible(modeIndex != 0);
		break;
	case 2:
		mServer->setCommand(3);
		ui->tableTypeCmBox->setCurrentIndex(3);
		ui->sendModeCBox->removeItem(1);
		ui->sendModeCBox->addItem("Прочитать регистр");
		ui->runMasterChekBx->setVisible(modeIndex == 0);
		ui->registerNumberEdit->setVisible(modeIndex != 0);
		ui->readRegisterButton->setVisible(modeIndex != 0);
		break;
	case 3:
		mServer->setCommand(4);
		ui->tableTypeCmBox->setCurrentIndex(2);
		ui->sendModeCBox->removeItem(1);
		ui->sendModeCBox->addItem("Прочитать регистр");
		ui->runMasterChekBx->setVisible(modeIndex == 0);
		ui->registerNumberEdit->setVisible(modeIndex != 0);
		ui->readRegisterButton->setVisible(modeIndex != 0);
		break;
	case 4:
		mServer->setCommand(5);
		ui->tableTypeCmBox->setCurrentIndex(0);
		ui->sendModeCBox->removeItem(1);
		ui->sendModeCBox->addItem("Отправлять при изменении");
		ui->runMasterChekBx->setVisible(true);
		ui->registerNumberEdit->setVisible(false);
		ui->readRegisterButton->setVisible(false);
		break;
	case 5:
		mServer->setCommand(6);
		ui->tableTypeCmBox->setCurrentIndex(3);
		ui->sendModeCBox->removeItem(1);
		ui->sendModeCBox->addItem("Отправлять при изменении");
		ui->runMasterChekBx->setVisible(true);
		ui->registerNumberEdit->setVisible(false);
		ui->readRegisterButton->setVisible(false);
		break;
	case 6:
		mServer->setCommand(15);
		ui->tableTypeCmBox->setCurrentIndex(0);
		ui->sendModeCBox->removeItem(1);
		ui->sendModeCBox->addItem("Отправлять при изменении");
		ui->runMasterChekBx->setVisible(true);
		ui->registerNumberEdit->setVisible(false);
		ui->readRegisterButton->setVisible(false);
		break;
	case 7:
		mServer->setCommand(16);
		ui->tableTypeCmBox->setCurrentIndex(3);
		ui->sendModeCBox->removeItem(1);
		ui->sendModeCBox->addItem("Отправлять при изменении");
		ui->runMasterChekBx->setVisible(true);
		ui->registerNumberEdit->setVisible(false);
		ui->readRegisterButton->setVisible(false);
		break;
	}
	ui->sendModeCBox->setCurrentIndex(modeIndex);
}

void MainWindow::slotOnSendModeChanged(int index)
{
	int commandIndex = ui->messageTypeCmBox->currentIndex();
	if (index == 0) {
		ui->runMasterChekBx->setEnabled(true);
		ui->runMasterChekBx->setVisible(true);
		ui->registerNumberEdit->setVisible(false);
		ui->readRegisterButton->setVisible(false);
		mServer->setMode(ModbusServer::cyclicExchange);
	} else {
		ui->runMasterChekBx->setEnabled(false);
		ui->runMasterChekBx->setChecked(false);
		mServer->setMode(ModbusServer::byChangeExchange);
		if (commandIndex < 4) {
			ui->runMasterChekBx->setVisible(false);
			ui->registerNumberEdit->setVisible(true);
			ui->readRegisterButton->setVisible(true);
		} else {
			ui->runMasterChekBx->setVisible(true);
			ui->registerNumberEdit->setVisible(false);
			ui->readRegisterButton->setVisible(false);
		}
	}
}

void MainWindow::slotOnMessageTransmitted(int command, int adres, int count)
{
	QString msg("Tx: cmd:");
	msg.append(QString::number(command)).append(" register:").append(QString::number(adres));
	msg.append(" count:").append(QString::number(count));
	statusTxLbl->setText(msg);
}

void MainWindow::slotOnMessageReceived(int command, int adres, int count)
{
	QString msg("Rx: cmd:");
	msg.append(QString::number(command)).append(" register:").append(QString::number(adres));
	msg.append(" count:").append(QString::number(count));
	statusRxLbl->setText(msg);
}

void MainWindow::on_actionCOM_port_triggered()
{
	ComPortSettings settings(this, mOpenedPort);
	settings.setModal(true);
	int result = settings.exec();
	if (result == QDialog::Accepted) {
		QSerialPort *newPort = settings.getSelectedPort();
		qDebug() << "New port from dialog. "
				 << "Port name: " << newPort->portName() <<
					", baud:" << newPort->baudRate() <<
					", data bits: " << newPort->dataBits() <<
					", parity bits: " << newPort->parity() <<
					", stop bits: " << newPort->stopBits() <<
					", is open: "<< newPort->isOpen();
		netThread->reconfigureCOMPort(newPort);
	}
}

void MainWindow::slotOnCOMPortChanged(QSerialPort *currentPort)
{
	if (currentPort != nullptr) {
		mOpenedPort = currentPort;
	}
}

void MainWindow::on_actionModbus_protocol_triggered()
{
	ModbusSettings settings(mDataTables->getDiscreteOutputSize(), mDataTables->getDiscreteInputSize(),
							mDataTables->getAnalogInputSize(), mDataTables->getAnalogOutputSize(),
							mServer->getDiscreteOutputMessageSize(), mServer->getDiscreteInputMessageSize(),
							mServer->getAnalogInputMessageSize(), mServer->getAnalogOutputMessageSize(),
							mServer->getMessageTimeoutMs(), mServer->getNoANswerTimeoutMs(),
							ModbusRTU::getHostAdres(), ModbusRTU::getSlaveAdres(),
							mServer->getUseInSlaveMode());
	settings.setModal(true);
	int result = settings.exec();
	if (result == QDialog::Accepted) {
		int discreteOutputSize = settings.getDiscreteOutputSize();
		int discreteInputSize = settings.getDiscreteInputSize();
		int analogInputSize = settings.getAnalogInputSize();
		int analogOutputSize = settings.getAnalogOutputSize();
		int discreteOutputMessageSize = settings.getDiscreteOutputMessageSize();
		int discreteInputMessageSize = settings.getDiscreteInputMessageSize();
		int analogInputMessageSize = settings.getAnalogInputMessageSize();
		int analogOutputMessageSize = settings.getAnalogOutputMessageSize();
		int transmitTimeout = settings.getTransmitionTimeout();
		int noAnswerTimeout = settings.getNoAnswertimeout();
		int remoteHostAdres = settings.getRemoteHostAdres();
		int slaveAdres = settings.getSlaveAdres();
		bool useSlave = settings.getUseInSlaveMode();

		bool resumeExchange = false;
		if (discreteOutputSize != mDataTables->getDiscreteOutputSize() ||
				discreteInputSize != mDataTables->getDiscreteInputSize() ||
				analogInputSize != mDataTables->getAnalogInputSize() ||
				analogOutputSize != mDataTables->getAnalogOutputSize()) {
			if (!mServer->isExchangeOnPause()) {
				resumeExchange = true;
				mServer->slotPauseExchange(Qt::Unchecked);
				mServer->waitExchangeStoped();
			}
		}
		if (discreteOutputSize != mDataTables->getDiscreteOutputSize()) {
			if (mServer->isExchangeOnPause()) {
				mDataTables->resizeDiscreteOutputTable(discreteOutputSize);
				mModel->slotResizeDataTable(DataTables::discreteOutput);
			}
		}
		if (discreteInputSize != mDataTables->getDiscreteInputSize()) {
			if (mServer->isExchangeOnPause()) {
				mDataTables->resizeDiscreteIntputTable(discreteInputSize);
				mModel->slotResizeDataTable(DataTables::discreteInput);
			}
		}
		if (analogInputSize != mDataTables->getAnalogInputSize()) {
			if (mServer->isExchangeOnPause()) {
				mDataTables->resizeAnalogInputTable(analogInputSize);
				mModel->slotResizeDataTable(DataTables::analogInput);
			}
		}
		if (analogOutputSize != mDataTables->getAnalogOutputSize()) {
			if (mServer->isExchangeOnPause()) {
				mDataTables->resizeAnalogOutputTable(analogOutputSize);
				mModel->slotResizeDataTable(DataTables::analogOutput);
			}
		}
		if (resumeExchange) {
			mServer->slotPauseExchange(Qt::Checked);
		}
		if (discreteOutputMessageSize != mServer->getDiscreteOutputMessageSize()) {
			mServer->setDiscreteOutputMessageSize(discreteOutputMessageSize);
		}
		if (discreteInputMessageSize != mServer->getDiscreteInputMessageSize()) {
			mServer->setDiscreteInputMessageSize(discreteInputMessageSize);
		}
		if (analogInputMessageSize != mServer->getAnalogInputMessageSize()) {
			mServer->setAnalogInputMessageSize(analogInputMessageSize);
		}
		if (analogOutputMessageSize != mServer->getAnalogOutputMessageSize()) {
			mServer->setAnalogOutputMessageSize(analogOutputMessageSize);
		}
		if (transmitTimeout != mServer->getMessageTimeoutMs()) {
			if (transmitTimeout >= noAnswerTimeout) {
				noAnswerTimeout = transmitTimeout + 10;
			}
			mServer->setTransmitTimout(transmitTimeout);
		}
		if (noAnswerTimeout != mServer->getNoANswerTimeoutMs()) {
			if (transmitTimeout >= noAnswerTimeout) {
				transmitTimeout = noAnswerTimeout - 10;
				mServer->setTransmitTimout(transmitTimeout);
			}
			mServer->setNoAnswerTimout(noAnswerTimeout);
		}
		if (remoteHostAdres != ModbusRTU::getHostAdres() &&
				(remoteHostAdres >= 0) && (remoteHostAdres < 256)) {
			ModbusRTU::setHostAdres(static_cast<char>(remoteHostAdres));
		}
		if (slaveAdres != ModbusRTU::getSlaveAdres() &&
				(slaveAdres >= 0) && (slaveAdres < 256)) {
			ModbusRTU::setSlaveAdres(static_cast<char>(slaveAdres));
		}
		if (useSlave != mServer->getUseInSlaveMode()) {
			mServer->setUseInSlaveMode(useSlave);
		}
	}
}

void MainWindow::on_readRegisterButton_clicked()
{
	int index = ui->messageTypeCmBox->currentIndex();
	bool adresPresent = false;
	int adres = ui->registerNumberEdit->text().toInt(&adresPresent);
	if (!adresPresent) {
		return;
	}
	switch (index) {
	case 0:
		mServer->slotSendSingleData(1, adres, 1);
		break;
	case 1:
		mServer->slotSendSingleData(2, adres, 1);
		break;
	case 2:
		mServer->slotSendSingleData(3, adres, 1);
		break;
	case 3:
		mServer->slotSendSingleData(4, adres, 1);
		break;
	}
}

void MainWindow::on_registerNumberEdit_returnPressed()
{
	on_readRegisterButton_clicked();
}

void MainWindow::writeConfigFile()
{
	QFile file(fileConfig);
	if(file.open(QIODevice::WriteOnly | QIODevice::Text)) {
		QSerialPort::StopBits bits = mOpenedPort->stopBits();
		qDebug() << "StopBits: " << bits;
		QString toFile = TAG_PORT + ":" + mOpenedPort->portName() + "\n";
		toFile.append(TAG_BAUD + ":" + QString::number(mOpenedPort->baudRate()) + "\n")
				.append(TAG_DATA_BITS + ":" + QString::number(mOpenedPort->dataBits()) + "\n")
				.append(TAG_PARITY + ":" + QString::number(mOpenedPort->parity()) + "\n")
				.append(TAG_STOP_BITS + ":" + QString::number(bits) + "\n")
				.append(TAG_DO_TABLE_SIZE + ":" + QString::number(mDataTables->getDiscreteOutputSize()) + "\n")
				.append(TAG_DI_TABLE_SIZE + ":" + QString::number(mDataTables->getDiscreteInputSize()) + "\n")
				.append(TAG_AO_TABLE_SIZE + ":" + QString::number(mDataTables->getAnalogOutputSize()) + "\n")
				.append(TAG_AI_TABLE_SIZE + ":" + QString::number(mDataTables->getAnalogInputSize()) + "\n")
				.append(TAG_DO_MESS_SIZE + ":" + QString::number(mServer->getDiscreteOutputMessageSize()) + "\n")
				.append((TAG_DI_MESS_SIZE + ":" + QString::number(mServer->getDiscreteInputMessageSize()) + "\n"))
				.append(TAG_AO_MESS_SIZE + ":" + QString::number(mServer->getAnalogOutputMessageSize()) + "\n")
				.append(TAG_AI_MESS_SIZE + ":" + QString::number(mServer->getAnalogInputMessageSize()) + "\n")
				.append(TAG_DELAY + ":" + QString::number(mServer->getMessageTimeoutMs()) + "\n")
				.append(TAG_ANSWER_TIMEOUT + ":" + QString::number(mServer->getNoANswerTimeoutMs()) + "\n")
				.append(TAG_REMOTE_HOST + ":" + QString::number(ModbusRTU::getHostAdres()) + "\n")
				.append(TAG_SLAVE_ADRES + ":" + QString::number(ModbusRTU::getSlaveAdres()) + "\n");
		if (mServer->getUseInSlaveMode()) {
			toFile.append(TAG_USE_SLAVE + ":true");
		} else {
			toFile.append(TAG_USE_SLAVE + ":false");
		}
		QTextStream stream(&file);
		stream << toFile;
		file.close();
	} else {
		qDebug() << "File open error";
	}
}

void MainWindow::readConfigFile()
{
	QFile file(fileConfig);
	if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
		while (!file.atEnd()) {
			QByteArray bArr = file.readLine();
			QString line(bArr);
			QStringList strList = line.split(":");
			if (strList.size() > 1) {
				if (strList.at(0).compare(TAG_BAUD) == 0) {
					bool ok;
					qint32 baud = strList.at(1).toInt(&ok);
					if (ok) { mBaudRate = baud; }
				} else if (strList.at(0).compare(TAG_DATA_BITS) == 0) {
					bool ok;
					qint32 data = strList.at(1).toInt(&ok);
					if (ok) { mDataBits = data; }
				} else if (strList.at(0).compare(TAG_PARITY) == 0) {
					bool ok;
					qint32 p = strList.at(1).toInt(&ok);
					if (ok) { mParity = p; }
				} else if (strList.at(0).compare(TAG_STOP_BITS) == 0) {
					bool ok;
					qint32 s = strList.at(1).toInt(&ok);
					if (ok) { mStopBits = s; }
				} else if (strList.at(0).compare(TAG_PORT) == 0) {
					mPortName = strList.at(1).trimmed();
				} else if (strList.at(0).compare(TAG_DO_TABLE_SIZE) == 0) {
					bool ok;
					int size = strList.at(1).toInt(&ok);
					if (ok) { mDigOutTableSize = size; }
				} else if (strList.at(0).compare(TAG_DI_TABLE_SIZE) == 0) {
					bool ok;
					int size = strList.at(1).toInt(&ok);
					if (ok) { mDigInTableSize = size; }
				} else if (strList.at(0).compare(TAG_AO_TABLE_SIZE) == 0) {
					bool ok;
					int size = strList.at(1).toInt(&ok);
					if (ok) { mAnOutTableSize = size; }
				} else if (strList.at(0).compare(TAG_AI_TABLE_SIZE) == 0) {
					bool ok;
					int size = strList.at(1).toInt(&ok);
					if (ok) { mAnInTableSize = size; }
				} else if (strList.at(0).compare(TAG_DO_MESS_SIZE) == 0) {
					bool ok;
					int size = strList.at(1).toInt(&ok);
					if (ok) { mDigOutMessSize = size; }
				} else if (strList.at(0).compare(TAG_DI_MESS_SIZE) == 0) {
					bool ok;
					int size = strList.at(1).toInt(&ok);
					if (ok) { mDigInMessSize = size; }
				} else if (strList.at(0).compare(TAG_AO_MESS_SIZE) == 0) {
					bool ok;
					int size = strList.at(1).toInt(&ok);
					if (ok) { mAnOutMessSize = size; }
				} else if (strList.at(0).compare(TAG_AI_MESS_SIZE) == 0) {
					bool ok;
					int size = strList.at(1).toInt(&ok);
					if (ok) { mAnInMessSize = size; }
				} else if (strList.at(0).compare(TAG_DELAY) == 0) {
					bool ok;
					int d = strList.at(1).toInt(&ok);
					if (ok) { mDelayBetweenMess = d; }
				} else if (strList.at(0).compare(TAG_ANSWER_TIMEOUT) == 0) {
					bool ok;
					int t = strList.at(1).toInt(&ok);
					if (ok) { mNoAnswTimeout = t; }
				} else if (strList.at(0).compare(TAG_REMOTE_HOST) == 0) {
					bool ok;
					int a = strList.at(1).toInt(&ok);
					if (ok) { mRemoteAdres = a; }
				} else if (strList.at(0).compare(TAG_SLAVE_ADRES) == 0) {
					bool ok;
					int a = strList.at(1).toInt(&ok);
					if (ok) { mSlaveAdres = a; }
				} else if (strList.at(0).compare(TAG_USE_SLAVE) == 0) {
					if (strList.at(1).trimmed().compare("true") == 0) {
						mUseSlave = true;
					} else if (strList.at(1).trimmed().compare("false") == 0) {
						mUseSlave = false;
					}
				}
			}
		}
		file.close();
	}
}
