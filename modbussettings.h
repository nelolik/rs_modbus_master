#ifndef MODBUSSETTINGS_H
#define MODBUSSETTINGS_H
#include <QDialog>
#include <QObject>

class QLineEdit;
class QCheckBox;

class ModbusSettings : public QDialog
{
public:
	ModbusSettings(int discretOutputSize, int discretInputSize,
				   int analogIntputSize, int analogOutputSize,
				   int discretOutputMessageSize, int discretInputMessageSize,
				   int analogIntputMessageSize, int analogOutputMessageSize,
				   int transmitionTimeout, int noAnswerTimeout,
				   int remoteHostAdres, int slaveAdres,
				   bool useInSlaveMode);

	int getDiscreteOutputSize();
	int getDiscreteInputSize();
	int getAnalogInputSize();
	int getAnalogOutputSize();
	int getDiscreteOutputMessageSize();
	int getDiscreteInputMessageSize();
	int getAnalogInputMessageSize();
	int getAnalogOutputMessageSize();
	int getTransmitionTimeout();
	int getNoAnswertimeout();
	int getRemoteHostAdres();
	int getSlaveAdres();
	bool getUseInSlaveMode();

private:
	QLineEdit *editDiscreteOutputSize;
	QLineEdit *editDiscreteInputSize;
	QLineEdit *editAnalogInputSize;
	QLineEdit *editAnalogOutputSize;
	QLineEdit *editDiscreteOutputMessageSize;
	QLineEdit *editDiscreteInputMessageSize;
	QLineEdit *editAnalogInputMessageSize;
	QLineEdit *editAnalogOutputMessageSize;
	QLineEdit *editTransmitionTimeout;
	QLineEdit *editNoAnswerTimeout;
	QLineEdit *editHostAdres;
	QLineEdit *editSlaveAdres;
	QCheckBox *answerAsSlaveCBx;
};

#endif // MODBUSSETTINGS_H
