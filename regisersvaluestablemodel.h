#ifndef REGISERSVALUESTABLE_H
#define REGISERSVALUESTABLE_H
#include <QAbstractTableModel>
#include "datatables.h"

class RegisersValuesTableModel : public QAbstractTableModel
{
	Q_OBJECT
public:

	RegisersValuesTableModel(DataTables *tables, DataTables::TableType tableType);
	void setOutputTableType(DataTables::TableType type);



private:


	DataTables *mTables;
	DataTables::TableType mTableType;
	int mTableWidth;
	int mTableSize;

	int getCellValue(int position) const;
	void replaceCellValue(int position, int value);

public slots:
	void onDataChangedFronRS(DataTables::TableType type, uint position, uint count);
	void slotResizeDataTable(DataTables::TableType type);

signals:
	void signalUpdateRemoteData(int command, int adres, int count);

	// QAbstractItemModel interface
public:
	int rowCount(const QModelIndex &parent) const;
	int columnCount(const QModelIndex &parent) const;
	QVariant data(const QModelIndex &index, int role) const;
	bool setData(const QModelIndex &index, const QVariant &value, int role);
	Qt::ItemFlags flags(const QModelIndex &index) const;
	QVariant headerData(int section, Qt::Orientation orientation, int role) const;
};



#endif // REGISERSVALUESTABLE_H
