#ifndef TABLEITEMDELEGATE_H
#define TABLEITEMDELEGATE_H
#include <QItemDelegate>

class TableItemDelegate : public QItemDelegate
{
public:
	TableItemDelegate();

	// QAbstractItemDelegate interface
public:
	QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;
};

#endif // TABLEITEMDELEGATE_H
