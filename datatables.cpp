#include "datatables.h"
#include <QBitArray>
#include <QVector>

DataTables::DataTables()
{
	pDiscreteOutput = new QBitArray();
	pDiscreteInput = new QBitArray();
	pAnalogInput = new QVector<int>();
	pAnalogOutput = new QVector<int>();
}

DataTables::DataTables(int discretOutputSize, int discretInputSize,
					   int analogIntputSize, int analogOutputSize)

{
	pDiscreteOutput = new QBitArray(discretOutputSize);
	pDiscreteInput = new QBitArray(discretInputSize);
	pAnalogInput = new QVector<int>(analogIntputSize);
	pAnalogOutput = new QVector<int>(analogOutputSize);
	tablesDescritons << "Discrete Output Coils" << "Discrete Input Contacts"
					 << "Analog Input Registers" << "Analog Output Holding Registers";

	for (int i = 0; i < analogIntputSize; i++) {
		(*pAnalogInput)[i] = 0;//i + 3000;
	}
	for (int i = 0; i < analogOutputSize; i++) {
		(*pAnalogOutput)[i] = 0;//i + 4000;
	}
}

DataTables::~DataTables()
{
	delete pDiscreteOutput;
	delete pDiscreteInput;
	delete pAnalogInput;
	delete pAnalogOutput;
}

int DataTables::getDiscreteOutputCell(int number)
{
	 bool cell = (number >= 0) && (number < pDiscreteOutput->size()) ?
				 pDiscreteOutput->at(number) : false;
	 return cell ? 1 : 0;
}

bool DataTables::setDiscreteOutputCell(int number, int val)
{
	if (pDiscreteOutput == nullptr || number < 0 || number >= pDiscreteOutput->size()) {
		return false;
	}
	pDiscreteOutput->setBit(number, val == 0 ? false : true);
	return true;
}

void DataTables::resizeDiscreteOutputTable(int size)
{
	pDiscreteOutput->resize(size);
}

int DataTables::getDiscreteInputCell(int number)
{
	bool cell = (number >= 0) && (number < pDiscreteInput->size()) ?
				pDiscreteInput->at(number) : false;
	return cell ? 1 : 0;
}

int DataTables::setDiscreteIntputCell(int number, int val)
{
	if ((pDiscreteInput == nullptr) || (number < 0) || (number >= pDiscreteInput->size())) {
		return false;
	}
	pDiscreteInput->setBit(number, val == 0? false: true);
	return true;
}

void DataTables::resizeDiscreteIntputTable(int size)
{
	pDiscreteInput->resize(size);
}

int DataTables::getAnalogInputCell(int number)
{
	return (pAnalogInput != nullptr) && (number >= 0) && (number < pAnalogInput->size()) ?
				pAnalogInput->at(number) : 0;
}

bool DataTables::setAnalogInputCell(int number, int val)
{
	if ((pAnalogInput == nullptr) || (number < 0) || (number >= pAnalogInput->size())) {
		return false;
	}
	pAnalogInput->replace(number, val);
	return true;
}

void DataTables::resizeAnalogInputTable(int size)
{
	pAnalogInput->resize(size);
}

int DataTables::getAnalogOutputCell(int number)
{
	return (pAnalogOutput != nullptr) && (number >= 0) && (number < pAnalogOutput->size()) ?
				pAnalogOutput->at(number) : 0;
}

bool DataTables::setAnalogOutputCell(int number, int val)
{
	if ((pAnalogOutput == nullptr) || (number < 0) || (number >= pAnalogOutput->size())) {
		return false;
	}
	pAnalogOutput->replace(number, val);
	return true;
}

void DataTables::resizeAnalogOutputTable(int size)
{
	pAnalogOutput->resize(size);
}
