#ifndef MODBUSRTU_H
#define MODBUSRTU_H
#include <QVector>

class DataTables;
class NetworkThread;
class QByteArray;

class ModbusRTU
{

public:
	ModbusRTU(NetworkThread *netThread, DataTables *tables);

	QByteArray cmd1ReadCoilStatusReceive(char netAdres, int startRegister, int count);
	QByteArray cmd1ReadCoilStatusSend(int adres, int count);
	bool cmd1ReadCoilStatusReceiveAnswer(int adres, int count, QByteArray &message);

	QByteArray cmd2ReadInputStatusReceive(char netAdres, int startRegister, int count);
	QByteArray cmd2ReadInputStatusSend(int adres, int count);
	bool cmd2ReadInputStatusReceiveAnswer(int adres, int count, QByteArray &message);

	QByteArray cmd3ReadHoldingRegsReceive(char netAdres, int startRegister, int count);
	QByteArray cmd3ReadHoldingRegsSend(int adres, int count);
	void cmd3ReadHoldingRegsReceiveAnswer(int startAdres, QByteArray &message);

	QByteArray cmd4ReadInputRegsReceive(char netAdres, int startRegister, int count);
	QByteArray cmd4ReadInputRegsSend(int adres, int count);
	void cmd4ReadInputRegsReceiveAnswer(int startAdres, QByteArray &message);

	QByteArray cmd5ForceSingleCoilReceive(char netAdres, int startRegister, QByteArray &request);
	QByteArray cmd5ForceSingleCoilSend(int adres);
	bool cmd5ForceSingleCoilReceiveAnswer(int adres, QByteArray &message);

	QByteArray cmd6PresetSinglRegisterReceive(char netAdres, int startRegister, QByteArray &request);
	QByteArray cmd6PresetSinglRegisterSend(int adres);
	bool cmd6PresetSinglRegisterReceiveAnswer(int adres, QByteArray &message);

	QByteArray cmd15ForceMultipleCoilsReceive(char netAdres, int startRegister, int count, QByteArray &message);
	QByteArray cmd15ForceMultipleCoilsSend(int adres, int count);
	bool cmd15ForceMultipleCoilsReceiveAnswer(int adres, int count, QByteArray &message);

	QByteArray cmd16PresetMultipleRegsReceive(char netAdres, int startRegister, int count, QByteArray &message);
	QByteArray cmd16PresetMultipleRegsSend(int adres, int count);
	bool cmd16PresetMultipleRegsReceiveAnswer(int adres, int count, QByteArray &message);

	static QByteArray checkIsValidMessage(QByteArray &bytes);
	static char getSlaveAdres() { return slaveAdres; }
	static char getHostAdres() { return requestedAdres; }
	static void setSlaveAdres(char adres) { slaveAdres = adres; }
	static void setHostAdres(char adres) { requestedAdres = adres; }

private:
	static char slaveAdres;
	static char requestedAdres;
	NetworkThread *pNetThread;
	DataTables *mTables;
	static QVector<int> mCommands;
	static QVector<int> mShortSlaveCommands;
	static QVector<int> mFixedMasterAnswers;
	static bool isMaster;

	static bool checkCrcEquals(QByteArray &bytes, int crcByteNumber);
	uint calcCRC(QByteArray &bytes);

#define HIBYTE(x) static_cast<char>(((x) >> 8) & 0xFF)
#define LOBYTE(x) static_cast<char>((x) & 0xFF)

};

#endif // MODBUSRTU_H
