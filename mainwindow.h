#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}
class NetworkThread;
class DataTables;
class ModbusServer;
class RegisersValuesTableModel;
class QSerialPort;
class QLabel;

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = nullptr);
	~MainWindow();

private:
	Ui::MainWindow *ui;
	QLabel *statusTxLbl;
	QLabel *statusRxLbl;
	NetworkThread *netThread;
	DataTables *mDataTables;
	ModbusServer *mServer;
	RegisersValuesTableModel *mModel;
	QSerialPort *mOpenedPort;
	bool mChangeTableSize;

	QString mPortName;
	qint32 mBaudRate;
	qint32 mDataBits;
	qint32 mParity;
	qint32 mStopBits;
	int mDigOutTableSize;
	int mDigInTableSize;
	int mAnOutTableSize;
	int mAnInTableSize;
	int mDigOutMessSize;
	int mDigInMessSize;
	int mAnOutMessSize;
	int mAnInMessSize;
	int mDelayBetweenMess;
	int mNoAnswTimeout;
	int mRemoteAdres;
	int mSlaveAdres;
	bool mUseSlave;

	static QString fileConfig;
	static QString TAG_PORT;
	static QString TAG_BAUD;
	static QString TAG_DATA_BITS;
	static QString TAG_PARITY;
	static QString TAG_STOP_BITS;
	static QString TAG_DO_TABLE_SIZE;
	static QString TAG_DI_TABLE_SIZE;
	static QString TAG_AO_TABLE_SIZE;
	static QString TAG_AI_TABLE_SIZE;
	static QString TAG_DO_MESS_SIZE;
	static QString TAG_DI_MESS_SIZE;
	static QString TAG_AO_MESS_SIZE;
	static QString TAG_AI_MESS_SIZE;
	static QString TAG_DELAY;
	static QString TAG_ANSWER_TIMEOUT;
	static QString TAG_REMOTE_HOST;
	static QString TAG_SLAVE_ADRES;
	static QString TAG_USE_SLAVE;

	void writeConfigFile();
	void readConfigFile();

public slots:
//	void onActionPortSelected();
	void onRSError(QString);	//Unused
	void onRSData(QString);		//Unused
	void slotOnTableTypeChanged(int index);
	void slotOnSendCommandTypeChanged(int index);
	void slotOnSendModeChanged(int index);
	void slotOnMessageTransmitted(int command, int adres, int count);
	void slotOnMessageReceived(int command, int adres, int count);
private slots:
	void on_actionCOM_port_triggered();
	void slotOnCOMPortChanged(QSerialPort *currentPort);
	void on_actionModbus_protocol_triggered();
	void on_readRegisterButton_clicked();
	void on_registerNumberEdit_returnPressed();
};

#endif // MAINWINDOW_H
