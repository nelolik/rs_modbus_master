#include "regisersvaluestablemodel.h"
#include "datatables.h"

RegisersValuesTableModel::RegisersValuesTableModel(DataTables *tables, DataTables::TableType tableType)
{
	mTables = tables;
	mTableSize = 0;
	mTableWidth = 16;
	setOutputTableType(tableType);
}

void RegisersValuesTableModel::setOutputTableType(DataTables::TableType type)
{
	beginResetModel();
	mTableType = type;
	switch (mTableType) {
	case DataTables::discreteOutput:
		mTableSize = mTables->getDiscreteOutputSize();
		break;
	case DataTables::discreteInput:
		mTableSize = mTables->getDiscreteInputSize();
		break;
	case DataTables::analogInput:
		mTableSize = mTables->getAnalogInputSize();
		break;
	case DataTables::analogOutput:
		mTableSize = mTables->getAnalogOutputSize();
		break;
	}
	endResetModel();
}

int RegisersValuesTableModel::getCellValue(int position) const
{
	int value = 0;
	switch (mTableType) {
	case DataTables::discreteOutput:
		value = mTables->getDiscreteOutputCell(position);
		break;
	case DataTables::discreteInput:
		value = mTables->getDiscreteInputCell(position);
		break;
	case DataTables::analogInput:
		value = mTables->getAnalogInputCell(position);
		break;
	case DataTables::analogOutput:
		value = mTables->getAnalogOutputCell(position);
		break;
	}
	return value;
}

void RegisersValuesTableModel::replaceCellValue(int position, int value)
{
	if (position >= mTableSize) {
		return;
	}
	switch (mTableType) {
	case DataTables::discreteOutput:
		mTables->setDiscreteOutputCell(position, value);
		emit signalUpdateRemoteData(5, position, 1);
		break;
	case DataTables::discreteInput:
		mTables->setDiscreteIntputCell(position, value);
		break;
	case DataTables::analogInput:
		mTables->setAnalogInputCell(position, value);
		break;
	case DataTables::analogOutput:
		mTables->setAnalogOutputCell(position, value);
		emit signalUpdateRemoteData(6, position, 1);
		break;
	}
}

void RegisersValuesTableModel::onDataChangedFronRS(DataTables::TableType type, uint position, uint count)
{

	if(type != mTableType) {
		return;
	}
	int startRow = (static_cast<int>(position) % mTableWidth) != 0 ?
				static_cast<int>(position) / mTableWidth :
				static_cast<int>(position) / mTableWidth + 1;
	int startColumn = static_cast<int>(position) % mTableWidth;
	int endPosition = static_cast<int>(position + count);
	switch (type) {
	case DataTables::discreteOutput:
		if (endPosition >= mTables->getDiscreteOutputSize()) { endPosition -= mTables->getDiscreteOutputSize();}
		break;
	case DataTables::discreteInput:
		if (endPosition >= mTables->getDiscreteInputSize()) { endPosition -= mTables->getDiscreteInputSize();}
		break;
	case DataTables::analogInput:
		if (endPosition >= mTables->getAnalogInputSize()) { endPosition -= mTables->getAnalogInputSize();}
		break;
	case DataTables::analogOutput:
		if (endPosition >= mTables->getAnalogOutputSize()) { endPosition -= mTables->getAnalogOutputSize();}
		break;
	}

	int endRow = (static_cast<int>(endPosition) % mTableWidth) != 0 ?
				static_cast<int>(endPosition) / mTableWidth :
				static_cast<int>(endPosition) / mTableWidth + 1;
	int endColumn = static_cast<int>(endPosition) % mTableWidth;
	if (startRow < endRow) {
		dataChanged(index(startRow, startColumn), index(endRow, endColumn));
	} else {
		dataChanged(index(0, 0), index(endRow, endColumn));
		dataChanged(index(startRow, startColumn), index(rowCount(QModelIndex()) - 1, columnCount(QModelIndex()) - 1));
	}
}

void RegisersValuesTableModel::slotResizeDataTable(DataTables::TableType type)
{

	if (type == mTableType) {
		beginResetModel();
		switch (mTableType) {
		case DataTables::discreteOutput:
			mTableSize = mTables->getDiscreteOutputSize();
			break;
		case DataTables::discreteInput:
			mTableSize = mTables->getDiscreteInputSize();
			break;
		case DataTables::analogInput:
			mTableSize = mTables->getAnalogInputSize();
			break;
		case DataTables::analogOutput:
			mTableSize = mTables->getAnalogOutputSize();
			break;
		}
		endResetModel();
	}
}


int RegisersValuesTableModel::rowCount(const QModelIndex &parent) const
{
	Q_UNUSED(parent)
	if (mTableSize % mTableWidth == 0) {
		return mTableSize / mTableWidth;
	} else {
		return mTableSize / mTableWidth + 1;
	}
}

int RegisersValuesTableModel::columnCount(const QModelIndex &parent) const
{
	Q_UNUSED(parent)
	return mTableWidth;
}

QVariant RegisersValuesTableModel::data(const QModelIndex &index, int role) const
{
	if (!index.isValid() || mTables == nullptr) {
		return QVariant();
	}
	int row = index.row();
	int column = index.column();
	if (role == Qt::DisplayRole || role == Qt::EditRole) {
		int position = row * mTableWidth + column;
		if (position >= mTableSize) return QVariant();
		return getCellValue(position);
	}


	return QVariant();
}

bool RegisersValuesTableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
	if (!index.isValid()) {
		return false;
	}
	if (role == Qt::EditRole) {
		int row = index.row();
		int column = index.column();
		int position = row * mTableWidth + column;
		if (position > mTableSize) {
			return false;
		}
		replaceCellValue(position, value.toInt());
	}
	return false;
}

Qt::ItemFlags RegisersValuesTableModel::flags(const QModelIndex &index) const
{
	Qt::ItemFlags flags = QAbstractTableModel::flags(index);
	if(!index.isValid() )
		return flags;
	flags |= Qt::ItemIsEditable;
	return flags;
}


QVariant RegisersValuesTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
	{
		return section;
	}
	if (orientation == Qt::Vertical && role == Qt::DisplayRole)
	{
		int firstInRow = 16 * section;
		int lastInRow = 16 * (section + 1) - 1;
		return QString::number(firstInRow).append("-").append(QString::number(lastInRow));
	}
	return QVariant();
}
