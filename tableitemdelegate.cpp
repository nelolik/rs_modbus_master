#include "tableitemdelegate.h"
#include <QLineEdit>

TableItemDelegate::TableItemDelegate()
{

}


QWidget *TableItemDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	return new QLineEdit(parent);
}
