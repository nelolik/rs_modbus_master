#include "modbusrtu.h"
#include "crc.h"
#include "networkthread.h"
#include "datatables.h"
#include <QByteArray>
#include <QVector>
#include <QDebug>


ModbusRTU::ModbusRTU(NetworkThread *netThread, DataTables *tables)
{
	pNetThread = netThread;
	mTables = tables;
	slaveAdres = 1;
	requestedAdres = 2;
	isMaster = true;
	mCommands << 1 << 2 << 3 << 4 << 5 << 6 << 15 << 16;
	mShortSlaveCommands << 1 << 2 << 3 << 4 << 5 << 6;
	mFixedMasterAnswers << 5 << 6 << 15 << 16;
}

char ModbusRTU::slaveAdres = 1;
char ModbusRTU::requestedAdres = 2;
bool ModbusRTU::isMaster = false;
QVector<int> ModbusRTU::mCommands;// << 1 << 2 << 3 << 4 << 5 << 6 << 15 << 16;
QVector<int> ModbusRTU::mShortSlaveCommands;// << 1 << 2 << 3 << 4 << 5 << 6;
QVector<int> ModbusRTU::mFixedMasterAnswers;// << 5 << 6 << 15 << 16;

QByteArray ModbusRTU::cmd1ReadCoilStatusReceive(char netAdres, int startRegister, int count)
{
	QByteArray message;
	if (startRegister >= mTables->getDiscreteOutputSize()) {
		return message;
	}
	message.append(netAdres);
	message.append(1);	//command
	QByteArray values;
	char byte = 0;
	for (int i = 0; i < count; ++i) {
		int displacment = i % 8;
		int cellNumber = startRegister + i;
		if (cellNumber >= mTables->getDiscreteOutputSize()) {
			cellNumber -= mTables->getDiscreteOutputSize();
		}
		int value = mTables->getDiscreteOutputCell(cellNumber);
		byte |= value << displacment;
		if (displacment == 7 || i == (count - 1)) {
			values.append(byte);
			byte = 0;
		}
	}
	message.append(static_cast<char>(values.size()));
	message.append(values);
	uint crc = calcCRC(message);
	message.append(LOBYTE(crc));
	message.append(HIBYTE(crc));
	return message;
}

QByteArray ModbusRTU::cmd1ReadCoilStatusSend(int adres, int count)
{
	QByteArray bytes;
	bytes.append(requestedAdres);
	bytes.append(1);	//command
	bytes.append(HIBYTE(adres));
	bytes.append(LOBYTE(adres));
	bytes.append(HIBYTE(count));
	bytes.append(LOBYTE(count));
	uint crc = calcCRC(bytes);
	bytes.append(LOBYTE(crc));
	bytes.append(HIBYTE(crc));
	return bytes;
}

bool ModbusRTU::cmd1ReadCoilStatusReceiveAnswer(int adres, int count, QByteArray &message)
{
	int bytesCount = message[2];
	int cellNum = adres;
	for (int b = 0; b < bytesCount; ++b) {
		uchar byte = static_cast<uchar>(message[b + 3]);
		for (int i = 0; i < 8 && (cellNum < (count + adres)); ++i) {
			int value = (byte >> i) & 1;
			if(cellNum >= mTables->getDiscreteOutputSize()) {
				return false;
			}
			mTables->setDiscreteOutputCell(cellNum++, value);
		}
	}
	return true;
}

QByteArray ModbusRTU::cmd2ReadInputStatusReceive(char netAdres, int startRegister, int count)
{
	QByteArray message;
	if (startRegister >= mTables->getDiscreteInputSize()) {
		return message;
	}
	message.append(netAdres);
	message.append(2);	//command
	QByteArray values;
	char byte = 0;
	for (int i = 0; i < count; ++i) {
		int displacment = i % 8;
		int cellNumber = startRegister + i;
		if (cellNumber >= mTables->getDiscreteInputSize()) {
			cellNumber -= mTables->getDiscreteInputSize();
		}
		int value = mTables->getDiscreteInputCell(cellNumber);
		byte |= value << displacment;
		if (displacment == 7 || i == (count - 1)) {
			values.append(byte);
			byte = 0;
		}
	}
	message.append(static_cast<char>(values.size()));
	message.append(values);
	uint crc = calcCRC(message);
	message.append(LOBYTE(crc));
	message.append(HIBYTE(crc));
	return message;
}

QByteArray ModbusRTU::cmd2ReadInputStatusSend(int adres, int count)
{
	QByteArray bytes;
	bytes.append(requestedAdres);
	bytes.append(2);	//command
	bytes.append(HIBYTE(adres));
	bytes.append(LOBYTE(adres));
	bytes.append(HIBYTE(count));
	bytes.append(LOBYTE(count));
	uint crc = calcCRC(bytes);
	bytes.append(LOBYTE(crc));
	bytes.append(HIBYTE(crc));
	return bytes;
}

bool ModbusRTU::cmd2ReadInputStatusReceiveAnswer(int adres, int count, QByteArray &message)
{
	int bytesCount = message[2];
	int cellNum = adres;
	for (int b = 0; b < bytesCount; ++b) {
		uchar byte = static_cast<uchar>(message[b + 3]);
		for (int i = 0; i < 8 && (cellNum < (count + adres)); ++i) {
			int value = (byte >> i) & 1;
			if(cellNum >= mTables->getDiscreteInputSize()) {
				return false;
			}
			mTables->setDiscreteIntputCell(cellNum++, value);
		}
	}
	return true;
}

// In slave mode
QByteArray ModbusRTU::cmd3ReadHoldingRegsReceive(char netAdres, int startRegister, int count)
{
	QByteArray message;
	int currentRegister = startRegister;
	if (mTables->getAnalogOutputSize() == 0) {
		return message;
	}
	message.append(netAdres).append(3).append(static_cast<char>(count * 2));
	while (count-- > 0) {
		if (currentRegister >= mTables->getAnalogOutputSize()) {
			currentRegister = 0;
		}
		uint value = static_cast<uint>(mTables->getAnalogOutputCell(currentRegister++));
		message.append(static_cast<char>((value >> 8) & 0xFF))
				.append(static_cast<char>(value & 0xFF));
	}
	uint crc = calcCRC(message);
	message.append(static_cast<char>(crc & 0xFF))
			.append(static_cast<char>((crc >> 8) & 0xFF));

	return message;
}

QByteArray ModbusRTU::cmd3ReadHoldingRegsSend(int adres, int count)
{
	QByteArray bytes;
	bytes.append(requestedAdres);
	bytes.append(3);	//command
	bytes.append(HIBYTE(adres));
	bytes.append(LOBYTE(adres));
	bytes.append(HIBYTE(count));
	bytes.append(LOBYTE(count));
	uint crc = calcCRC(bytes);
	bytes.append(LOBYTE(crc));
	bytes.append(HIBYTE(crc));
	return bytes;
}

void ModbusRTU::cmd3ReadHoldingRegsReceiveAnswer(int startAdres, QByteArray &message)
{
	int registersCount = message[2] / 2;
	for (int i = 0; i < registersCount; ++i) {
		int value = (static_cast<uint>(static_cast<uchar>(message[i * 2 + 3])) << 8) & 0xFF00;
		value |= static_cast<uint>(static_cast<uchar>(message[i * 2 + 3 + 1])) & 0xFF;
		mTables->setAnalogOutputCell(startAdres + i, value);
	}
}

QByteArray ModbusRTU::cmd4ReadInputRegsReceive(char netAdres, int startRegister, int count)
{
	QByteArray message;
	int currentRegister = startRegister;
	if (mTables->getAnalogInputSize() == 0) {
		return message;
	}
	message.append(netAdres).append(4).append(static_cast<char>(count * 2));
	while (count-- > 0) {
		if (currentRegister >= mTables->getAnalogOutputSize()) {
			currentRegister = 0;
		}
		uint value = static_cast<uint>(mTables->getAnalogInputCell(currentRegister++));
		message.append(static_cast<char>((value >> 8) & 0xFF))
				.append(static_cast<char>(value & 0xFF));
	}
	uint crc = calcCRC(message);
	message.append(static_cast<char>(crc & 0xFF))
			.append(static_cast<char>((crc >> 8) & 0xFF));

	return message;
}

QByteArray ModbusRTU::cmd4ReadInputRegsSend(int adres, int count)
{
	QByteArray bytes;
	bytes.append(requestedAdres);
	bytes.append(4);	//command
	bytes.append(HIBYTE(adres));
	bytes.append(LOBYTE(adres));
	bytes.append(HIBYTE(count));
	bytes.append(LOBYTE(count));
	uint crc = calcCRC(bytes);
	bytes.append(LOBYTE(crc));
	bytes.append(HIBYTE(crc));
	return bytes;
}

void ModbusRTU::cmd4ReadInputRegsReceiveAnswer(int startAdres, QByteArray &message)
{
	int registersCount = message[2] / 2;
	for (int i = 0; i < registersCount; ++i) {
		int value = (static_cast<uint>(static_cast<uchar>(message[i * 2 + 3])) << 8) & 0xFF00;
		value |= static_cast<uint>(static_cast<uchar>(message[i * 2 + 3 + 1])) & 0xFF;
		mTables->setAnalogInputCell(startAdres + i, value);
	}
}

QByteArray ModbusRTU::cmd5ForceSingleCoilReceive(char netAdres, int startRegister, QByteArray &request)
{
	QByteArray message;
	if (mTables->getDiscreteOutputSize() == 0 || request.size() < 6) {
		return message;
	}
	int value = (static_cast<uint>(static_cast<uchar>(request[4])) << 8) & 0xFF00;
	value |= static_cast<uint>(static_cast<uchar>(request[5])) & 0xFF;
	message.append(netAdres).append(5).append(HIBYTE(startRegister))
			.append(LOBYTE(startRegister));
	if (value != 0) {
		mTables->setDiscreteOutputCell(startRegister, 1);
		message.append(static_cast<char>(0xFF)).append(static_cast<char>(0));
	} else {
		mTables->setDiscreteOutputCell(startRegister, 0);
		message.append(static_cast<char>(0)).append(static_cast<char>(0));
	}
	uint crc = calcCRC(message);
	message.append(LOBYTE(crc))
			.append(HIBYTE(crc));
	return message;
}

QByteArray ModbusRTU::cmd5ForceSingleCoilSend(int adres)
{
	QByteArray bytes;
	bytes.append(requestedAdres);
	bytes.append(5);	//command
	bytes.append(HIBYTE(adres));
	bytes.append(LOBYTE(adres));
	int value = mTables->getDiscreteOutputCell(adres);
	if (value == 0) {
		bytes.append(static_cast<char>(0));
		bytes.append(static_cast<char>(0));
	} else {
		bytes.append(static_cast<char>(0xFF));
		bytes.append(static_cast<char>(0));
	}

	uint crc = calcCRC(bytes);
	bytes.append(LOBYTE(crc));
	bytes.append(HIBYTE(crc));
	return bytes;
}

bool ModbusRTU::cmd5ForceSingleCoilReceiveAnswer(int adres, QByteArray &message)
{
	int receivedAdres = (static_cast<uint>(static_cast<uchar>(message[2])) << 8) & 0xFF00;
	receivedAdres |= static_cast<uint>(static_cast<uchar>(message[3])) & 0xFF;
	if (adres == receivedAdres) {
		int val = (static_cast<uint>(static_cast<uchar>(message[4])) << 8) & 0xFF00;
		val |= static_cast<uint>(static_cast<uchar>(message[5])) & 0xFF;
		if (val == 0xFF00) {
			val = 1;
		}
		if (val == 0) {
			val = 0;
		}
		if (val == mTables->getDiscreteOutputCell(adres)) {
			return true;
		}
	}
	return false;
}

QByteArray ModbusRTU::cmd6PresetSinglRegisterReceive(char netAdres, int startRegister, QByteArray &request)
{
	QByteArray message;
	if (mTables->getAnalogInputSize() == 0 || request.size() < 6) {
		return message;
	}
	int value = (static_cast<uint>(static_cast<uchar>(request[4])) << 8) & 0xFF00;
	value |= static_cast<uint>(static_cast<uchar>(request[5])) & 0xFF;
	message.append(netAdres).append(6)
			.append(HIBYTE(startRegister))
			.append(LOBYTE(startRegister));
	mTables->setAnalogOutputCell(startRegister, value);
	message.append(HIBYTE(value)).append(LOBYTE(value));
	uint crc = calcCRC(message);
	message.append(LOBYTE(crc))
			.append(HIBYTE(crc));
	return message;
}

QByteArray ModbusRTU::cmd6PresetSinglRegisterSend(int adres)
{
	QByteArray bytes;
	bytes.append(requestedAdres);
	bytes.append(6);	//command
	bytes.append(HIBYTE(adres));
	bytes.append(LOBYTE(adres));
	int value = mTables->getAnalogOutputCell(adres);
	bytes.append(HIBYTE(value));
	bytes.append(LOBYTE(value));
	uint crc = calcCRC(bytes);
	bytes.append(LOBYTE(crc));
	bytes.append(HIBYTE(crc));
	return bytes;
}

bool ModbusRTU::cmd6PresetSinglRegisterReceiveAnswer(int adres, QByteArray &message)
{
	int receivedAdres = (static_cast<uint>(static_cast<uchar>(message[2])) << 8) & 0xFF00;
	receivedAdres |= static_cast<uint>(static_cast<uchar>(message[3])) & 0xFF;
	if (adres == receivedAdres) {
		int val = (static_cast<uint>(static_cast<uchar>(message[4])) << 8) & 0xFF00;
		val |= static_cast<uint>(static_cast<uchar>(message[5])) & 0xFF;
		if (val == mTables->getAnalogOutputCell(adres)) {
			return true;
		}
	}
	return false;
}

QByteArray ModbusRTU::cmd15ForceMultipleCoilsReceive(char netAdres, int startRegister, int count, QByteArray &message)
{
	QByteArray answer;
	int currentRegister = startRegister;
	if (message.size() < static_cast<int>(count / 8 + 7)) {
		return answer;
	}
	int dataByteNumber = 7;
	for (int i = 0; i < count; ++i) {
		if (currentRegister >= mTables->getDiscreteOutputSize()) {
			currentRegister = 0;
		}
		int displacement = i % 8;
		mTables->setDiscreteOutputCell(currentRegister++, (message[dataByteNumber] >> displacement) & 1);
		if (displacement == 7) {
			dataByteNumber += 1;
		}

	}

	answer.append(netAdres).append(15).append(HIBYTE(startRegister))
			.append(LOBYTE(startRegister))
			.append(HIBYTE(count))
			.append(LOBYTE(count));
	uint crc = calcCRC(answer);
	answer.append(LOBYTE(crc))
			.append(HIBYTE(crc));
	return answer;
}

QByteArray ModbusRTU::cmd15ForceMultipleCoilsSend(int adres, int count)
{
	QByteArray message;
	message.append(requestedAdres);
	message.append(15);	//command
	message.append(HIBYTE(adres));
	message.append(LOBYTE(adres));
	message.append(HIBYTE(count));
	message.append(LOBYTE(count));
	QByteArray values;
	char byte = 0;
	for (int i = 0; i < count; ++i) {
		int displacment = i % 8;
		int cellNumber = adres + i;
		if (cellNumber >= mTables->getDiscreteOutputSize()) {
			cellNumber -= mTables->getDiscreteOutputSize();
		}
		int value = mTables->getDiscreteOutputCell(cellNumber);
		byte |= value << displacment;
		if (displacment == 7 || i == (count - 1)) {
			values.append(byte);
			byte = 0;
		}
	}
	message.append(static_cast<char>(values.size()));
	message.append(values);
	uint crc = calcCRC(message);
	message.append(LOBYTE(crc));
	message.append(HIBYTE(crc));
	return message;
}

bool ModbusRTU::cmd15ForceMultipleCoilsReceiveAnswer(int adres, int count, QByteArray &message)
{
	int receivedAdres = (static_cast<uint>(static_cast<uchar>(message[2])) << 8) & 0xFF00;
	receivedAdres |= static_cast<uint>(static_cast<uchar>(message[3])) & 0xFF;
	if (adres == receivedAdres) {
		int receivedCount = (static_cast<uint>(static_cast<uchar>(message[4])) << 8) & 0xFF00;
		receivedCount |= static_cast<uint>(static_cast<uchar>(message[5])) & 0xFF;
		if (count == receivedCount) {
			return true;
		}
	}
	return false;
}

QByteArray ModbusRTU::cmd16PresetMultipleRegsReceive(char netAdres, int startRegister, int count, QByteArray &message)
{
	QByteArray answer;
	int currentRegister = startRegister;
	if (message.size() < static_cast<int>(count * 2 + 7)) {
		return answer;
	}
	for (int i = 0; i < count; i++) {
		if (currentRegister >= mTables->getAnalogOutputSize()) {
			currentRegister = 0;
		}
		int value = (static_cast<uint>(static_cast<uchar>(message[i * 2 + 7])) << 8) & 0xFF00;
		value |= static_cast<uint>(static_cast<uchar>(message[i * 2 + 8])) & 0xFF;
		mTables->setAnalogOutputCell(currentRegister++, value);
	}
	answer.append(netAdres).append(16).append(HIBYTE(startRegister))
			.append(LOBYTE(startRegister))
			.append(HIBYTE(count))
			.append(LOBYTE(count));
	uint crc = calcCRC(answer);
	answer.append(static_cast<char>(crc & 0xFF))
			.append(static_cast<char>((crc >> 8) & 0xFF));
	return answer;
}

QByteArray ModbusRTU::cmd16PresetMultipleRegsSend(int adres, int count)
{
	QByteArray message;
	message.append(requestedAdres);
	message.append(16);	//command
	message.append(HIBYTE(adres));
	message.append(LOBYTE(adres));
	message.append(HIBYTE(count));
	message.append(LOBYTE(count));
	message.append(static_cast<char>(count * 2));
	for (int i = 0; i < count; ++i) {
		int cellNum = adres + i;
		if (cellNum >= mTables->getAnalogOutputSize()) {
			cellNum -= mTables->getAnalogOutputSize();
		}
		int value = mTables->getAnalogOutputCell(adres + i);
		message.append(HIBYTE(value));
		message.append(LOBYTE(value));
	}
	uint crc = calcCRC(message);
	message.append(LOBYTE(crc));
	message.append(HIBYTE(crc));
	return message;
}

bool ModbusRTU::cmd16PresetMultipleRegsReceiveAnswer(int adres, int count, QByteArray &message)
{
	int receivedAdres = (static_cast<uint>(static_cast<uchar>(message[2])) << 8) & 0xFF00;
	receivedAdres |= static_cast<uint>(static_cast<uchar>(message[3])) & 0xFF;
	if (adres == receivedAdres) {
		int receivedCount = (static_cast<uint>(static_cast<uchar>(message[4])) << 8) & 0xFF00;
		receivedCount |= static_cast<uint>(static_cast<uchar>(message[5])) & 0xFF;
		if (count == receivedCount) {
			return true;
		}
	}
	return false;
}

QByteArray ModbusRTU::checkIsValidMessage(QByteArray &bytes)
{
	while (bytes.size() >= 6) {	//size is smaller when answer
		if (bytes[0] == slaveAdres) {
			isMaster = false;
		} else if (bytes[0] == requestedAdres) {
			isMaster = true;
		} else {
			bytes.remove(0, 1);
			continue;
		}
		int cmdNumber = -1;
		if(mCommands.contains(bytes[1])) {
			cmdNumber = bytes[1];
		}
		if (cmdNumber == -1) {
			bytes.remove(0, 1);
			continue;
		}
		if (isMaster) {
			if (mFixedMasterAnswers.contains(cmdNumber)) {
				if (bytes.size() < 8) {
					return nullptr;
				}
				bool equals = checkCrcEquals(bytes, 6);
				if (equals) {
					QByteArray message = bytes.left(6);
					bytes.remove(0, 6);
					return message;
				} else {
					bytes.remove(0, 1);
				}
			} else {
				int countDataBytes = static_cast<uchar>(bytes[2]);
				if (bytes.size() < (5 + countDataBytes)) {
					return nullptr;
				}
				bool equals = checkCrcEquals(bytes, countDataBytes + 3);
				if (equals) {
					QByteArray message = bytes.left(countDataBytes + 3);
					bytes.remove(0, countDataBytes + 5);
					return message;
				} else {
					bytes.remove(0, 1);
				}
			}
		} else {
			if(mShortSlaveCommands.contains(cmdNumber)) {
				if (bytes.size() < 8) return nullptr;
				bool equals = checkCrcEquals(bytes, 6);
				if (equals) {
					QByteArray message = bytes.left(6);
					bytes.remove(0, 8);
					return message;
				} else {
					bytes.remove(0, 1);
				}
				return nullptr;
			} else if (cmdNumber == 15 || cmdNumber == 16) {
				int countDataBytes = static_cast<uchar>(bytes[6]);
				if (bytes.size() < (countDataBytes + 9)) return nullptr;
				bool equals = checkCrcEquals(bytes, countDataBytes + 7);
				if (equals) {
					QByteArray message = bytes.left(countDataBytes + 7);
					bytes.remove(0, countDataBytes + 9);
					return message;
				} else {
					bytes.remove(0, 1);
				}

				return nullptr;
			}
		}
	}


	return nullptr;
}

bool ModbusRTU::checkCrcEquals(QByteArray &bytes, int crcByteNumber)
{
	if (crcByteNumber <= 0 || crcByteNumber > (bytes.size() - 2)) {
		return false;
	}
	unsigned int buffer[crcByteNumber];
	for (int i = 0; i < crcByteNumber; i++) {
		buffer[i] = static_cast<unsigned int>(bytes[i]);
	}
	unsigned int crc = 0xffff;
	crc = Crc::getCRC16_IBM(crc, buffer, crcByteNumber);
	unsigned int receivedCrc = 0;
	receivedCrc |= static_cast<uint>(static_cast<uchar>(bytes[crcByteNumber]));
	receivedCrc |= (static_cast<uint>(static_cast<uchar>(bytes[crcByteNumber + 1])) << 8) & 0xFF00;
	if (crc == receivedCrc) return true;
	return false;
}

uint ModbusRTU::calcCRC(QByteArray &bytes)
{
	uint size = static_cast<uint>(bytes.size());
	if (size == 0) {
		return 0;
	}
	unsigned int buffer[size];
	for (uint i = 0; i < size; i++) {
		buffer[i] = static_cast<unsigned int>(bytes[i]);
	}
	unsigned int crc = 0xffff;
	crc = Crc::getCRC16_IBM(crc, buffer, size);
	return crc;
}


