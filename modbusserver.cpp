#include "modbusserver.h"
#include "modbusrtu.h"
#include "datatables.h"
#include "networkthread.h"
#include "QDebug"
#include <QWaitCondition>

ModbusServer::ModbusServer(DataTables *tables, NetworkThread *netThread, int commandNumber,
						   Exchange mode, bool startExchange,
						   int analogOutMsgSize, int analogInputMsgSize,
						   int discreteOutputMsgSize, int discreteInputMsgSize) :
	mRunning(true),
	mWaitingAnswer(false), mAnswerReceived(false), mPause(!startExchange), mUseInSlaveMode(true),
	mTimer(this),
	mutex(), mutexPause(), mutexWaitPause(),
	mMode(mode), mNewMode(mode)
{
	pNetThred = netThread;

	mTables = tables;
	mModbusRTU = new ModbusRTU(netThread, mTables);
	masterCommands << "01-Read Coil Status" << "02-Read Input Status"
				   << "03-Read Holding Registers" << "04-Read Input Registers"
				   << "05-Force Single Coil" << "06-Preset Single Register"
				   << "15-Force Multiple Coils" << "16-Preset Multiple Registers";
	mNoAnswerTimeoutMs = 5000;
	mMessageTimeoutMks = 200000;
	mStartAdres = 0;

	mCommand = commandNumber;
	mNewCommand = commandNumber;

	this->analogOutMsgSize = analogOutMsgSize;
	this->analogInputMsgSize = analogInputMsgSize;
	this->discreteOutputMsgSize = discreteOutputMsgSize;
	this->discreteInputMsgSize = discreteInputMsgSize;

	connect(&mTimer, &QTimer::timeout, this, &ModbusServer::slotOnTimeout);
	connect(this, &ModbusServer::signalUpdateTimer, this, &ModbusServer::slotOnUpdateTimer);
	connect(this, &ModbusServer::signalStopTimer, this, &ModbusServer::slotOnStopTimer);
	mTimer.setInterval(mNoAnswerTimeoutMs);

	connect(this, &ModbusServer::signalExchangeOnPause, this, &ModbusServer::slotExchangeStoped);

	if (mode == byChangeExchange) {
		mutex.lock();
	}
}

ModbusServer::~ModbusServer()
{
	mRunning = false;

}

void ModbusServer::stopRun()
{
	 mRunning = false;
	 mTimer.stop();
	 pauseCondition.notify_all();
	 waitPauseCondition.notify_all();
	 if (!mutex.tryLock()) {
		 mutex.unlock();
	 } else {
		 mutex.unlock();
	 }
}

void ModbusServer::setCommand(int command)
{
	mNewCommand = command;
}

void ModbusServer::setMode(ModbusServer::Exchange mode)
{
	mNewMode = mode;
	if (mPause) {
		pauseCondition.notify_all();
	}
}

void ModbusServer::waitExchangeStoped()
{
	qDebug() << "Waiting for pause";
	if(!mPause) {
		mutexWaitPause.lock();
		waitPauseCondition.wait(&mutexWaitPause);
		mutexWaitPause.unlock();
	}
	qDebug() << "Waiting finished";
}

void ModbusServer::slotReceiveBytes(QByteArray bytes)
{
	if (bytes == nullptr || bytes.size() < 4) {
		return;
	}
	char netAdres = bytes[0];
	uint command = static_cast<uint>(bytes[1]);
	if (mWaitingAnswer && (command == static_cast<uint>(mCommand))) {
		parseMasterAnswerData(static_cast<int>(command), bytes);
		mAnswerReceived = true;
		mWaitingAnswer = false;
		if (mMode == byChangeExchange) {
			mTimer.stop();
		}
		if (mMode == mNewMode) {
			mutex.unlock();
		}
	} else if(mUseInSlaveMode) {
		QByteArray	answerPackage = prepareAnswerData(netAdres, command, bytes);
		if (answerPackage.size() > 0) {
			pNetThred->setDataToTransmit(answerPackage);
		}
	}

}

void ModbusServer::slotOnTimeout()
{
	pNetThred->clearBuffer();
	if (!mutex.tryLock())
		mutex.unlock();
	qDebug() << "Server wait answer timeout";
}

void ModbusServer::slotOnUpdateTimer()
{
	mTimer.start(mNoAnswerTimeoutMs);
}

void ModbusServer::slotOnStopTimer()
{
	mTimer.stop();
}

void ModbusServer::slotSendSingleData(int command, int adres, int count)
{
	if (!mRunning || mMode != byChangeExchange) {
		return;
	}
	mNewCommand = command;
	mCommand = command;
	mStartAdres = adres;
	mSentMessageSize = count;
	pauseCondition.notify_all();
}

void ModbusServer::slotPauseExchange(int checkState)
{
	qDebug() << "Slot pause";
	if (mMode == cyclicExchange) {

		if (checkState == Qt::Checked) {
			mPause = false;
			pauseCondition.notify_all();
		} else {
			mPause = true;
		}
	}
}

void ModbusServer::slotExchangeStoped()
{
	waitPauseCondition.wakeAll();
}




void ModbusServer::run()
{
	while(mRunning) {
		usleep(static_cast<ulong>(mMessageTimeoutMks));

		mutex.lock();

		if (!mRunning) {
			mutex.unlock();
			break;
		}
		if (mMode != mNewMode) {
			mMode = mNewMode;
			if (mMode == cyclicExchange) {
				mStartAdres = 0;
				mAnswerReceived = false;
				mPause = true;
			} else {
				mAnswerReceived = true;
			}
		}
		if (mCommand != mNewCommand) {
			mCommand = mNewCommand;
			if (mMode == cyclicExchange) {
				mStartAdres = 0;
			}
			mAnswerReceived = false;
		}

		if (mPause && (mMode == cyclicExchange) && (mNewMode == cyclicExchange)) {
			qDebug() << "Enter pause";
			mutexPause.lock();
			emit signalStopTimer();
			emit signalExchangeOnPause();
			pauseCondition.wait(&mutexPause);
			mutexPause.unlock();
			qDebug() << "Exit pause";
			if ((mMode != mNewMode) ||(mCommand != mNewCommand)) {
				mutex.unlock();
				continue;
			}
		}
		if (mMode == byChangeExchange) {
			if (mAnswerReceived) {
				qDebug() << "Enter pause by change";
				mutexPause.lock();
				qDebug() << "Enter pause by change after lock";
				mPause = true;
				emit signalStopTimer();
				emit signalExchangeOnPause();
				pauseCondition.wait(&mutexPause);
				mPause = false;
				mutexPause.unlock();
				qDebug() << "Exit pause by change";
				if (mMode != mNewMode) {
					mutex.unlock();
					continue;
				}
			}
		}
		if (!mRunning) {
			break;
		}
		switch (mCommand) {
		case 1:
			sendMessageCmd1();
			break;
		case 2:
			sendMessageCmd2();
			break;
		case 3:
			sendMessageCmd3();
			break;
		case 4:
			sendMessageCmd4();
			break;
		case 5:
			sendMessageCmd5();
			break;
		case 6:
			sendMessageCmd6();
			break;
		case 15:
			sendMessageCmd15();
			break;
		case 16:
			sendMessageCmd16();
			break;
		}
		emit signalMessageTransmitted(mCommand, mStartAdres, mSentMessageSize);

		emit signalUpdateTimer();

	}
}

void ModbusServer::sendMessageCmd1()
{
	int tableSize = mTables->getDiscreteOutputSize();
	if (tableSize == 0) {
		return;
	}
	if (mMode == cyclicExchange) {
		adresAndCountConstraints(tableSize);
		mSentMessageSize = (discreteOutputMsgSize + mStartAdres) < tableSize ? discreteOutputMsgSize
																: tableSize - mStartAdres;
	} else {
		if ((mSentMessageSize + mStartAdres) < tableSize) {
			mSentMessageSize = tableSize - mStartAdres;
		}
	}

	QByteArray message = mModbusRTU->cmd1ReadCoilStatusSend(mStartAdres, mSentMessageSize);
	if (message.size() > 0) {
		pNetThred->setDataToTransmit(message);
		mWaitingAnswer = true;
	}
	qDebug() << "Command 1 send";
}

void ModbusServer::sendMessageCmd2()
{
	int tableSize = mTables->getDiscreteInputSize();
	if (tableSize == 0) {
		return;
	}
	if (mMode == cyclicExchange) {
		adresAndCountConstraints(tableSize);
		mSentMessageSize = (discreteInputMsgSize + mStartAdres) < tableSize ? discreteInputMsgSize
																: tableSize - mStartAdres;
	} else {
		if ((mSentMessageSize + mStartAdres) < tableSize) {
			mSentMessageSize = tableSize - mStartAdres;
		}
	}
	QByteArray message = mModbusRTU->cmd2ReadInputStatusSend(mStartAdres, mSentMessageSize);
	if (message.size() > 0) {
		pNetThred->setDataToTransmit(message);
		mWaitingAnswer = true;
	}
	qDebug() << "Command 2 send";
}

void ModbusServer::sendMessageCmd3()
{
	int tableSize = mTables->getAnalogOutputSize();
	if (tableSize == 0) {
		return;
	}
	if (mMode == cyclicExchange) {
		adresAndCountConstraints(tableSize);
		mSentMessageSize = (analogOutMsgSize + mStartAdres) < tableSize ? analogOutMsgSize
																: tableSize - mStartAdres;
	} else {
		if ((mSentMessageSize + mStartAdres) > tableSize) {
			mSentMessageSize = tableSize - mStartAdres;
		}
	}
	QByteArray message = mModbusRTU->cmd3ReadHoldingRegsSend(mStartAdres, mSentMessageSize);
	if (message.size() > 0) {
		pNetThred->setDataToTransmit(message);
		mWaitingAnswer = true;
	}
	qDebug() << "Command 3 send";
}

void ModbusServer::sendMessageCmd4()
{
	int tableSize = mTables->getAnalogInputSize();
	if (tableSize == 0) {
		return;
	}
	if (mMode == cyclicExchange) {
		adresAndCountConstraints(tableSize);
		mSentMessageSize = (analogInputMsgSize + mStartAdres) < tableSize ? analogInputMsgSize
																: tableSize - mStartAdres;
	} else {
		if ((mSentMessageSize + mStartAdres) > tableSize) {
			mSentMessageSize = tableSize - mStartAdres;
		}
	}
	QByteArray message = mModbusRTU->cmd4ReadInputRegsSend(mStartAdres, mSentMessageSize);
	if (message.size() > 0) {
		pNetThred->setDataToTransmit(message);
		mWaitingAnswer = true;
	}
	qDebug() << "Command 4 send";
}

void ModbusServer::sendMessageCmd5()
{
	int tableSize = mTables->getDiscreteOutputSize();
	if (tableSize == 0) {
		return;
	}
	if (mMode == cyclicExchange) {
		incrementAdres(tableSize);
	} else {
		if (mStartAdres >= tableSize) {
			return;
		}
	}
	mSentMessageSize = 1;
	QByteArray message = mModbusRTU->cmd5ForceSingleCoilSend(mStartAdres);
	if (message.size() > 0) {
		pNetThred->setDataToTransmit(message);
		mWaitingAnswer = true;
	}
	qDebug() << "Command 5 send";
}

void ModbusServer::sendMessageCmd6()
{
	int tableSize = mTables->getAnalogOutputSize();
	if (tableSize == 0) {
		return;
	}
	if (mMode == cyclicExchange) {
		incrementAdres(tableSize);
	} else {
		if (mStartAdres >= tableSize) {
			return;
		}
	}
	mSentMessageSize = 1;
	QByteArray message = mModbusRTU->cmd6PresetSinglRegisterSend(mStartAdres);
	if (message.size() > 0) {
		pNetThred->setDataToTransmit(message);
		mWaitingAnswer = true;
	}
	qDebug() << "Command 6 send";
}

void ModbusServer::sendMessageCmd15()
{
	int tableSize = mTables->getDiscreteOutputSize();
	if (tableSize == 0) {
		return;
	}
	if (mMode == cyclicExchange) {
		adresAndCountConstraints(tableSize);
		mSentMessageSize = (discreteOutputMsgSize + mStartAdres) < tableSize ? discreteOutputMsgSize
																: tableSize - mStartAdres;
	} else {
		if ((mSentMessageSize + mStartAdres) < tableSize) {
			mSentMessageSize = tableSize - mStartAdres;
		}
	}
	QByteArray message = mModbusRTU->cmd15ForceMultipleCoilsSend(mStartAdres, mSentMessageSize);
	if (message.size() > 0) {
		pNetThred->setDataToTransmit(message);
		mWaitingAnswer = true;
	}
	qDebug() << "Command 15 send";
}

void ModbusServer::sendMessageCmd16()
{
	int tableSize = mTables->getAnalogOutputSize();
	if (tableSize == 0) {
		return;
	}
	if (mMode == cyclicExchange) {
		adresAndCountConstraints(tableSize);
		mSentMessageSize = (analogOutMsgSize + mStartAdres) < tableSize ? analogOutMsgSize
																: tableSize - mStartAdres;
	} else {
		if ((mSentMessageSize + mStartAdres) < tableSize) {
			mSentMessageSize = tableSize - mStartAdres;
		}
	}
	QByteArray message = mModbusRTU->cmd16PresetMultipleRegsSend(mStartAdres, mSentMessageSize);
	if (message.size() > 0) {
		pNetThred->setDataToTransmit(message);
		mWaitingAnswer = true;
	}
	qDebug() << "Command 16 send";
}

void ModbusServer::parseMasterAnswerData(int cmdNumber, QByteArray &message)
{
	switch (cmdNumber) {
	case 1:
		mModbusRTU->cmd1ReadCoilStatusReceiveAnswer(mStartAdres, mSentMessageSize, message);
		emit signalDataChanged(DataTables::discreteOutput, mStartAdres, mSentMessageSize);
		break;
	case 2:
		mModbusRTU->cmd2ReadInputStatusReceiveAnswer(mStartAdres, mSentMessageSize, message);
		emit signalDataChanged(DataTables::discreteInput, mStartAdres, mSentMessageSize);
		break;
	case 3:
		mModbusRTU->cmd3ReadHoldingRegsReceiveAnswer(mStartAdres, message);
		emit signalDataChanged(DataTables::analogOutput, mStartAdres, mSentMessageSize);
		break;
	case 4:
		mModbusRTU->cmd4ReadInputRegsReceiveAnswer(mStartAdres, message);
		emit signalDataChanged(DataTables::analogInput, mStartAdres, mSentMessageSize);
		break;
	case 5:
		mModbusRTU->cmd5ForceSingleCoilReceiveAnswer(mStartAdres, message);
		break;
	case 6:
		mModbusRTU->cmd6PresetSinglRegisterReceiveAnswer(mStartAdres, message);
		break;
	case 15:
		mModbusRTU->cmd15ForceMultipleCoilsReceiveAnswer(mStartAdres, mSentMessageSize, message);
		break;
	case 16:
		mModbusRTU->cmd16PresetMultipleRegsReceiveAnswer(mStartAdres, mSentMessageSize, message);
		break;
	}
	emit signalMessageRecieved(cmdNumber, mStartAdres, mSentMessageSize);
}

QByteArray ModbusServer::prepareAnswerData(char netAdres, uint cmdNumber, QByteArray &message)
{
	int startAdres = (static_cast<uint>((static_cast<uchar>(message[2]))) << 8) & 0xFF00;
	startAdres |= ((static_cast<uchar>(message[3]) & 0xFF));
	int registersCount = 1;
	if ((cmdNumber < 5) || (cmdNumber > 6)) {
		registersCount = (static_cast<uint>((static_cast<uchar>(message[4])) << 8)) & 0xFF00;
		registersCount |= ((static_cast<uchar>(message[5]) & 0xFF));
	}
	emit signalMessageRecieved(cmdNumber, startAdres, registersCount);
	QByteArray answer;
	switch (cmdNumber) {
	case 1:
		answer = mModbusRTU->cmd1ReadCoilStatusReceive(netAdres, startAdres, registersCount);
		break;
	case 2:
		answer = mModbusRTU->cmd2ReadInputStatusReceive(netAdres, startAdres, registersCount);
		break;
	case 3:
		answer = mModbusRTU->cmd3ReadHoldingRegsReceive(netAdres, startAdres, registersCount);
		break;
	case 4:
		answer = mModbusRTU->cmd4ReadInputRegsReceive(netAdres, startAdres, registersCount);
		break;
	case 5: {
		answer = mModbusRTU->cmd5ForceSingleCoilReceive(netAdres, startAdres, message);
		emit signalDataChanged(DataTables::discreteOutput, startAdres, 1);
		break;
	}
	case 6: {
		answer = mModbusRTU->cmd6PresetSinglRegisterReceive(netAdres, startAdres, message);
		emit signalDataChanged(DataTables::analogOutput, startAdres, 1);
		break;
	}
	case 15: {
		answer = mModbusRTU->cmd15ForceMultipleCoilsReceive(netAdres, startAdres, registersCount, message);
		emit signalDataChanged(DataTables::discreteOutput, startAdres, registersCount);
		break;
	}
	case 16: {
		answer = mModbusRTU->cmd16PresetMultipleRegsReceive(netAdres, startAdres, registersCount, message);
		emit signalDataChanged(DataTables::analogOutput, startAdres, registersCount);
		break;
	}
	}
	if (answer.size() > 0) {
		emit signalMessageTransmitted(cmdNumber, startAdres, registersCount);
	}
	return answer;
}

void ModbusServer::adresAndCountConstraints(int tableSize)
{
	if (mAnswerReceived) {
		mStartAdres += mSentMessageSize;
		mAnswerReceived = false;
	}
	if (mStartAdres >= tableSize) {
		mStartAdres -= tableSize;
	}
}

void ModbusServer::incrementAdres(int tableSize)
{
	if (mAnswerReceived) {
		mStartAdres += 1;
		mAnswerReceived = false;
	}
	if (mStartAdres >= tableSize) {
		mStartAdres = 0;
	}
}
