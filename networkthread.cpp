#include "networkthread.h"
#include "modbusrtu.h"
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QByteArray>
#include <QDebug>
#include <QMutex>

NetworkThread::NetworkThread(QSerialPort *port)
{
	mStopRun = false;
	mWriteData = false;
	reconfigurePort = false;
	mClearBuffer = false;
	mPort = port;
	mNewPort = nullptr;
}

NetworkThread::~NetworkThread()
{
	mPort->close();
	delete mPort;
}

void NetworkThread::stopRun()
{
	mStopRun = true;
}

void NetworkThread::setDataToTransmit(QByteArray &data)
{
	mData.clear();
	mData.append(data);
	mWriteData = true;
}

QString NetworkThread::getPortName()
{
	return mPort->portName();
}

void NetworkThread::reconfigureCOMPort(QSerialPort *newPort)
{
	mNewPort = newPort;
	reconfigurePort = true;
}

void NetworkThread::clearBuffer()
{
	mClearBuffer = true;
}

void NetworkThread::run()
{
	if (!mPort->open(QIODevice::ReadWrite)) {
		qDebug() << "Could not  open port";
		return;
	}
	QByteArray data;
	data.reserve(50);
	while(!mStopRun) {

		if (reconfigurePort) {
			if (mNewPort != nullptr) {
				mPort->close();
				delete mPort;
				mPort = mNewPort;
				if (!mPort->open(QIODevice::ReadWrite)) {
					qDebug() << "Could not  open port";
					return;
				}
				mNewPort = nullptr;
				reconfigurePort = false;
				emit signalCOMPortChanged(mPort);
			}
		}

		if (mWriteData) {
			qint64 result = mPort->write(mData);
			if (result != mData.size()) {
				qDebug() << "Number of sended date not equials prepared data";
			}
//			qDebug() << "Write data to port" << mData;
			mWriteData = false;
			mPort->clear(QSerialPort::Input);
		} else if (mPort->waitForReadyRead(200)) {
			QByteArray buff = mPort->readAll();
			data.append(buff);
			//qDebug() << data;
			QByteArray message = ModbusRTU::checkIsValidMessage(data);
			if (message != nullptr) {
				emit signalRecievedBytes(message);
			}
		} else {
			QByteArray buff = mPort->readAll();
			data.append(buff);
			QByteArray message = ModbusRTU::checkIsValidMessage(data);
			if (message != nullptr) {
				emit signalRecievedBytes(message);
			}
		}
	}
//	qDebug() << dataLog;
}
