#-------------------------------------------------
#
# Project created by QtCreator 2019-07-29T10:55:25
#
#-------------------------------------------------

QT       += core gui serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = RSModbusTerminal
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    networkthread.cpp \
    datatables.cpp \
    modbusrtu.cpp \
    modbusserver.cpp \
    crc.cpp \
    regisersvaluestablemodel.cpp \
    tableitemdelegate.cpp \
    comportsettings.cpp \
    modbussettings.cpp

HEADERS += \
        mainwindow.h \
    networkthread.h \
    datatables.h \
    modbusrtu.h \
    modbusserver.h \
    crc.h \
    regisersvaluestablemodel.h \
    tableitemdelegate.h \
    comportsettings.h \
    modbussettings.h

FORMS += \
        mainwindow.ui

OBJECTS_DIR = ./tmp
MOC_DIR = ./tmp

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

win32:CONFIG(release, debug|release): LIBS += -LC:/Qt/5.9.3/mingw53_32/lib/ -lQt5SerialPort
else:win32:CONFIG(debug, debug|release): LIBS += -LC:/Qt/5.9.3/mingw53_32/lib/ -lQt5SerialPortd
else:unix: LIBS += -LC:/Qt/5.9.3/mingw53_32/lib/ -lQt5SerialPort

INCLUDEPATH += C:/Qt/5.9.3/mingw53_32/include
DEPENDPATH += C:/Qt/5.9.3/mingw53_32/include

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += C:/Qt/5.9.3/mingw53_32/lib/libQt5SerialPort.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += C:/Qt/5.9.3/mingw53_32/lib/libQt5SerialPortd.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += C:/Qt/5.9.3/mingw53_32/lib/Qt5SerialPort.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += C:/Qt/5.9.3/mingw53_32/lib/Qt5SerialPortd.lib
else:unix: PRE_TARGETDEPS += C:/Qt/5.9.3/mingw53_32/lib/libQt5SerialPort.a
