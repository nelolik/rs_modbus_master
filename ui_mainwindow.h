/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTableView>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionmenu1;
    QAction *actionCOM_port;
    QAction *actionModbus_protocol;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *modbusLayout;
    QLabel *masterLbl;
    QComboBox *messageTypeCmBox;
    QComboBox *sendModeCBox;
    QCheckBox *runMasterChekBx;
    QLineEdit *registerNumberEdit;
    QPushButton *readRegisterButton;
    QSpacerItem *horizontalSpacer;
    QFrame *line;
    QHBoxLayout *tableChooseLayout;
    QComboBox *tableTypeCmBox;
    QSpacerItem *horizontalSpacer_2;
    QTableView *tableOutput;
    QMenuBar *menuBar;
    QMenu *menuPort;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(800, 500);
        actionmenu1 = new QAction(MainWindow);
        actionmenu1->setObjectName(QStringLiteral("actionmenu1"));
        actionCOM_port = new QAction(MainWindow);
        actionCOM_port->setObjectName(QStringLiteral("actionCOM_port"));
        actionModbus_protocol = new QAction(MainWindow);
        actionModbus_protocol->setObjectName(QStringLiteral("actionModbus_protocol"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout_2 = new QVBoxLayout(centralWidget);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        modbusLayout = new QHBoxLayout();
        modbusLayout->setSpacing(6);
        modbusLayout->setObjectName(QStringLiteral("modbusLayout"));
        masterLbl = new QLabel(centralWidget);
        masterLbl->setObjectName(QStringLiteral("masterLbl"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(masterLbl->sizePolicy().hasHeightForWidth());
        masterLbl->setSizePolicy(sizePolicy);

        modbusLayout->addWidget(masterLbl);

        messageTypeCmBox = new QComboBox(centralWidget);
        messageTypeCmBox->setObjectName(QStringLiteral("messageTypeCmBox"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(messageTypeCmBox->sizePolicy().hasHeightForWidth());
        messageTypeCmBox->setSizePolicy(sizePolicy1);
        messageTypeCmBox->setMinimumSize(QSize(180, 0));

        modbusLayout->addWidget(messageTypeCmBox);

        sendModeCBox = new QComboBox(centralWidget);
        sendModeCBox->setObjectName(QStringLiteral("sendModeCBox"));

        modbusLayout->addWidget(sendModeCBox);

        runMasterChekBx = new QCheckBox(centralWidget);
        runMasterChekBx->setObjectName(QStringLiteral("runMasterChekBx"));

        modbusLayout->addWidget(runMasterChekBx);

        registerNumberEdit = new QLineEdit(centralWidget);
        registerNumberEdit->setObjectName(QStringLiteral("registerNumberEdit"));
        registerNumberEdit->setMaximumSize(QSize(40, 16777215));
        registerNumberEdit->setInputMethodHints(Qt::ImhDigitsOnly);

        modbusLayout->addWidget(registerNumberEdit);

        readRegisterButton = new QPushButton(centralWidget);
        readRegisterButton->setObjectName(QStringLiteral("readRegisterButton"));
        readRegisterButton->setMaximumSize(QSize(50, 16777215));

        modbusLayout->addWidget(readRegisterButton);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        modbusLayout->addItem(horizontalSpacer);


        verticalLayout_2->addLayout(modbusLayout);

        line = new QFrame(centralWidget);
        line->setObjectName(QStringLiteral("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        verticalLayout_2->addWidget(line);

        tableChooseLayout = new QHBoxLayout();
        tableChooseLayout->setSpacing(6);
        tableChooseLayout->setObjectName(QStringLiteral("tableChooseLayout"));
        tableChooseLayout->setContentsMargins(-1, 0, -1, 0);
        tableTypeCmBox = new QComboBox(centralWidget);
        tableTypeCmBox->setObjectName(QStringLiteral("tableTypeCmBox"));
        sizePolicy1.setHeightForWidth(tableTypeCmBox->sizePolicy().hasHeightForWidth());
        tableTypeCmBox->setSizePolicy(sizePolicy1);
        tableTypeCmBox->setMinimumSize(QSize(180, 0));

        tableChooseLayout->addWidget(tableTypeCmBox);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        tableChooseLayout->addItem(horizontalSpacer_2);


        verticalLayout_2->addLayout(tableChooseLayout);

        tableOutput = new QTableView(centralWidget);
        tableOutput->setObjectName(QStringLiteral("tableOutput"));
        tableOutput->setInputMethodHints(Qt::ImhDigitsOnly);
        tableOutput->horizontalHeader()->setProperty("showSortIndicator", QVariant(false));
        tableOutput->verticalHeader()->setProperty("showSortIndicator", QVariant(false));

        verticalLayout_2->addWidget(tableOutput);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 800, 21));
        menuPort = new QMenu(menuBar);
        menuPort->setObjectName(QStringLiteral("menuPort"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuPort->menuAction());
        menuPort->addAction(actionCOM_port);
        menuPort->addAction(actionModbus_protocol);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
        actionmenu1->setText(QApplication::translate("MainWindow", "menu1", Q_NULLPTR));
        actionCOM_port->setText(QApplication::translate("MainWindow", "COM port", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionCOM_port->setShortcut(QApplication::translate("MainWindow", "Alt+C", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionModbus_protocol->setText(QApplication::translate("MainWindow", "Modbus protocol", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionModbus_protocol->setShortcut(QApplication::translate("MainWindow", "Alt+M", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        masterLbl->setText(QApplication::translate("MainWindow", "\320\232\320\276\320\274\320\260\320\275\320\264\320\260 \320\262 \321\200\320\265\320\266\320\270\320\274\320\265 \320\274\320\260\321\201\321\202\320\265\321\200\320\260", Q_NULLPTR));
        runMasterChekBx->setText(QApplication::translate("MainWindow", "\320\237\321\203\321\201\320\272", Q_NULLPTR));
        readRegisterButton->setText(QApplication::translate("MainWindow", "Read", Q_NULLPTR));
        menuPort->setTitle(QApplication::translate("MainWindow", "Settings", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
