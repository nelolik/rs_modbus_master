#ifndef NETWORKTHREAD_H
#define NETWORKTHREAD_H

#include <QObject>
#include <QThread>
#include <QMutex>

class QSerialPort;
class QSerialPortInfo;


class NetworkThread : public QThread
{
	Q_OBJECT
public:
	NetworkThread(QSerialPort *port);
	~NetworkThread();
	void stopRun();
	void setDataToTransmit(QByteArray &data);
	QString getPortName();
	void reconfigureCOMPort(QSerialPort *newPort);
	void clearBuffer();

private:
	QSerialPort *mPort;
	QSerialPort *mNewPort;
	bool mStopRun;
	bool mWriteData;
	bool reconfigurePort;
	bool mClearBuffer;
	QByteArray mData;
	QMutex serviceLock;
	// QThread interface
protected:
	void run();

signals:
	void signalError(QString);
	void signalRecievedData(QString);
	void signalRecievedBytes(QByteArray);
	void signalCOMPortChanged(QSerialPort *currentPort);
};

#endif // NETWORKTHREAD_H
